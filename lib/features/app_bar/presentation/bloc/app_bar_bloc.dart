import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'app_bar_event.dart';

part 'app_bar_state.dart';

part 'app_bar_bloc.freezed.dart';

@Singleton()
class AppBarBloc extends Bloc<AppBarEvent, AppBarState> {
  AppBarBloc() : super(AppBarState.initial()) {
    on<_$Push>((event, emit) async {
      emit(state.copyWith(widgets: event.widgets));
    });
    on<_$Clear>((event, emit) async {
      emit(AppBarState.initial());
    });
  }
}
