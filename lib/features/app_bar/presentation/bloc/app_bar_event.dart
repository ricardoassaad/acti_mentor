part of 'app_bar_bloc.dart';

@freezed
class AppBarEvent with _$AppBarEvent {
  const factory AppBarEvent.push(List<Widget> widgets) = Push;

  const factory AppBarEvent.clear() = Clear;
}
