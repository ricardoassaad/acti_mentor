part of 'app_bar_bloc.dart';

@freezed
class AppBarState with _$AppBarState {
  const factory AppBarState({
    required List<Widget> widgets,
  }) = _AppBarState;

  factory AppBarState.initial() => const AppBarState(
        widgets: [],
      );
}
