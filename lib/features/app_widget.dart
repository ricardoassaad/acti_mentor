import 'package:acti_mentor/features/app_bar/presentation/bloc/app_bar_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_add_form/categories_add_form_bloc.dart';
import 'package:acti_mentor/injection.dart';
import 'package:acti_mentor/routes/router.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage<bool>(
  name: 'AppRootRouter',
)
class AppWidgetScreen extends StatelessWidget {
  const AppWidgetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter.tabBar(
      physics: const NeverScrollableScrollPhysics(),
      routes: const [
        DashBoardRoute(),
        TabBarWidgetRouter(),
        TrackerHomeRoute(),
      ],
      builder: (context, child, controller) {
        final tabsRouter = AutoTabsRouter.of(context);
        return Scaffold(
          appBar: AppBar(
            title: const Text("Acti Mentor"),
            centerTitle: true,
            leading: IconButton(
              onPressed: () {
                //TODO: open drawer
              },
              icon: const Icon(Icons.menu),
            ),
            actions: [
              BlocBuilder<AppBarBloc, AppBarState>(
                builder: (context, state) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: state.widgets,
                  );
                },
              )
            ],
          ),
          body: child,
          // floatingActionButton: ExpandableFloatingActionButton(
          //   distance: 112.0,
          //   children: [
          //     ActionButton(
          //       onPressed: () {
          //         context.router.push(const WorkoutsAddRoute());
          //       },
          //       icon: const Icon(Icons.add),
          //     ),
          //     ActionButton(
          //       onPressed: () {
          //         context.router.push(const RoutinesAddRoute());
          //       },
          //       icon: const Icon(Icons.add),
          //     ),
          //     ActionButton(
          //       onPressed: () {
          //         context.router.push(const ExercisesAddRoute());
          //       },
          //       icon: const Icon(Icons.add),
          //     ),
          //     ActionButton(
          //       onPressed: () {
          //         getIt<CategoriesAddFormBloc>()
          //             .add(const CategoriesAddFormEvent.initialize());
          //         context.router.push(const CategoriesAddRoute());
          //       },
          //       icon: const Icon(Icons.add),
          //       text: "Add Category",
          //     ),
          //   ],
          // ),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                context.router.push(const CategoriesAddRoute());
                getIt<CategoriesAddFormBloc>()
                    .add(const CategoriesAddFormEvent.initialize());
              },
              child: const Icon(Icons.add)),
          // MultiFunctionFloatingButton(
          //   children: [
          //     ActionButton(
          //       onPressed: () {
          //         getIt<CategoriesAddFormBloc>()
          //             .add(const CategoriesAddFormEvent.initialize());
          //         context.router.push(const CategoriesAddRoute());
          //         getIt<MultiFunctionFloatingButtonBloc>()
          //             .add(const MultiFunctionFloatingButtonEvent.toggle());
          //       },
          //       icon: const Icon(Icons.add),
          //       text: "Add Category",
          //     ),
          //   ],
          // ),
          bottomNavigationBar: NavigationBar(
            selectedIndex: tabsRouter.activeIndex,
            onDestinationSelected: (index) {
              tabsRouter.setActiveIndex(index);
            },
            destinations: const [
              NavigationDestination(
                icon: Icon(Icons.dashboard),
                //TODO: make the label internationalized
                label: "Dashboard",
              ),
              NavigationDestination(
                icon: Icon(Icons.format_list_bulleted),
                //TODO: make the label internationalized
                label: "Workout Logs",
              ),
              NavigationDestination(
                icon: Icon(Icons.insert_chart),
                //TODO: make the label internationalized
                label: "Tracker",
              ),
            ],
          ),
        );
      },
    );
  }
}
