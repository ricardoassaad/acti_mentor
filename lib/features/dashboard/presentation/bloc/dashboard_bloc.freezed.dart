// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dashboard_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DashBoardEvent {
  Widget? get widget => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Widget? widget) update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Widget? widget)? update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Widget? widget)? update,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Update value) update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Update value)? update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Update value)? update,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DashBoardEventCopyWith<DashBoardEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashBoardEventCopyWith<$Res> {
  factory $DashBoardEventCopyWith(
          DashBoardEvent value, $Res Function(DashBoardEvent) then) =
      _$DashBoardEventCopyWithImpl<$Res, DashBoardEvent>;
  @useResult
  $Res call({Widget? widget});
}

/// @nodoc
class _$DashBoardEventCopyWithImpl<$Res, $Val extends DashBoardEvent>
    implements $DashBoardEventCopyWith<$Res> {
  _$DashBoardEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? widget = freezed,
  }) {
    return _then(_value.copyWith(
      widget: freezed == widget
          ? _value.widget
          : widget // ignore: cast_nullable_to_non_nullable
              as Widget?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UpdateCopyWith<$Res>
    implements $DashBoardEventCopyWith<$Res> {
  factory _$$UpdateCopyWith(_$Update value, $Res Function(_$Update) then) =
      __$$UpdateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Widget? widget});
}

/// @nodoc
class __$$UpdateCopyWithImpl<$Res>
    extends _$DashBoardEventCopyWithImpl<$Res, _$Update>
    implements _$$UpdateCopyWith<$Res> {
  __$$UpdateCopyWithImpl(_$Update _value, $Res Function(_$Update) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? widget = freezed,
  }) {
    return _then(_$Update(
      freezed == widget
          ? _value.widget
          : widget // ignore: cast_nullable_to_non_nullable
              as Widget?,
    ));
  }
}

/// @nodoc

class _$Update implements Update {
  const _$Update(this.widget);

  @override
  final Widget? widget;

  @override
  String toString() {
    return 'DashBoardEvent.update(widget: $widget)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Update &&
            (identical(other.widget, widget) || other.widget == widget));
  }

  @override
  int get hashCode => Object.hash(runtimeType, widget);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateCopyWith<_$Update> get copyWith =>
      __$$UpdateCopyWithImpl<_$Update>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Widget? widget) update,
  }) {
    return update(widget);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Widget? widget)? update,
  }) {
    return update?.call(widget);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Widget? widget)? update,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update(widget);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Update value) update,
  }) {
    return update(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Update value)? update,
  }) {
    return update?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Update value)? update,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update(this);
    }
    return orElse();
  }
}

abstract class Update implements DashBoardEvent {
  const factory Update(final Widget? widget) = _$Update;

  @override
  Widget? get widget;
  @override
  @JsonKey(ignore: true)
  _$$UpdateCopyWith<_$Update> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DashBoardState {
  Widget? get widget => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DashBoardStateCopyWith<DashBoardState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashBoardStateCopyWith<$Res> {
  factory $DashBoardStateCopyWith(
          DashBoardState value, $Res Function(DashBoardState) then) =
      _$DashBoardStateCopyWithImpl<$Res, DashBoardState>;
  @useResult
  $Res call({Widget? widget});
}

/// @nodoc
class _$DashBoardStateCopyWithImpl<$Res, $Val extends DashBoardState>
    implements $DashBoardStateCopyWith<$Res> {
  _$DashBoardStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? widget = freezed,
  }) {
    return _then(_value.copyWith(
      widget: freezed == widget
          ? _value.widget
          : widget // ignore: cast_nullable_to_non_nullable
              as Widget?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DashBoardStateCopyWith<$Res>
    implements $DashBoardStateCopyWith<$Res> {
  factory _$$_DashBoardStateCopyWith(
          _$_DashBoardState value, $Res Function(_$_DashBoardState) then) =
      __$$_DashBoardStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Widget? widget});
}

/// @nodoc
class __$$_DashBoardStateCopyWithImpl<$Res>
    extends _$DashBoardStateCopyWithImpl<$Res, _$_DashBoardState>
    implements _$$_DashBoardStateCopyWith<$Res> {
  __$$_DashBoardStateCopyWithImpl(
      _$_DashBoardState _value, $Res Function(_$_DashBoardState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? widget = freezed,
  }) {
    return _then(_$_DashBoardState(
      widget: freezed == widget
          ? _value.widget
          : widget // ignore: cast_nullable_to_non_nullable
              as Widget?,
    ));
  }
}

/// @nodoc

class _$_DashBoardState implements _DashBoardState {
  const _$_DashBoardState({this.widget});

  @override
  final Widget? widget;

  @override
  String toString() {
    return 'DashBoardState(widget: $widget)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DashBoardState &&
            (identical(other.widget, widget) || other.widget == widget));
  }

  @override
  int get hashCode => Object.hash(runtimeType, widget);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DashBoardStateCopyWith<_$_DashBoardState> get copyWith =>
      __$$_DashBoardStateCopyWithImpl<_$_DashBoardState>(this, _$identity);
}

abstract class _DashBoardState implements DashBoardState {
  const factory _DashBoardState({final Widget? widget}) = _$_DashBoardState;

  @override
  Widget? get widget;
  @override
  @JsonKey(ignore: true)
  _$$_DashBoardStateCopyWith<_$_DashBoardState> get copyWith =>
      throw _privateConstructorUsedError;
}
