part of 'dashboard_bloc.dart';

@freezed
class DashBoardEvent with _$DashBoardEvent {
  const factory DashBoardEvent.update(Widget? widget) = Update;
}
