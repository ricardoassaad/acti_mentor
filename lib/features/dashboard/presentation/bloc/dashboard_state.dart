part of 'dashboard_bloc.dart';

@freezed
class DashBoardState with _$DashBoardState {
  const factory DashBoardState({
    Widget? widget,
  }) = _DashBoardState;

  factory DashBoardState.initial() => const DashBoardState(
        widget: null,
      );
}
