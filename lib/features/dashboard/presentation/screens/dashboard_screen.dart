import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>()
class DashBoardScreen extends StatelessWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          children: const [
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: const [
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: const [
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: const [
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Card(
                margin: EdgeInsets.all(10),
                elevation: 2,
                child: SizedBox(
                  height: 150,
                  child: Center(
                    child: Text("tile 1"),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
