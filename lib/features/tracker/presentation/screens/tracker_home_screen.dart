import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>()
class TrackerHomeScreen extends StatelessWidget {
  const TrackerHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("Tracker"),
    );
  }
}
