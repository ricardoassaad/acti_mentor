import 'package:flutter/material.dart';

@immutable
class ActionButton extends StatelessWidget {
  const ActionButton({
    super.key,
    this.onPressed,
    required this.icon,
    this.text,
  });

  final VoidCallback? onPressed;
  final Widget icon;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Material(
      shape: const CircleBorder(),
      child: IconButton(
        onPressed: onPressed,
        icon: icon,
      ),
    );
  }
}
