import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'multi_function_floating_button_event.dart';

part 'multi_function_floating_button_state.dart';

part 'multi_function_floating_button_bloc.freezed.dart';

@Singleton()
class MultiFunctionFloatingButtonBloc extends Bloc<
    MultiFunctionFloatingButtonEvent, MultiFunctionFloatingButtonState> {
  MultiFunctionFloatingButtonBloc()
      : super(MultiFunctionFloatingButtonState.initial()) {
    on<_$Toggle>((event, emit) async {
      emit(state.copyWith(open: !state.open));
    });
  }
}
