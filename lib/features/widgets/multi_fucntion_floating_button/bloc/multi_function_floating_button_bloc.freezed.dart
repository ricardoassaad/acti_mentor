// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'multi_function_floating_button_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MultiFunctionFloatingButtonEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toggle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? toggle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toggle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Toggle value) toggle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Toggle value)? toggle,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Toggle value)? toggle,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultiFunctionFloatingButtonEventCopyWith<$Res> {
  factory $MultiFunctionFloatingButtonEventCopyWith(
          MultiFunctionFloatingButtonEvent value,
          $Res Function(MultiFunctionFloatingButtonEvent) then) =
      _$MultiFunctionFloatingButtonEventCopyWithImpl<$Res,
          MultiFunctionFloatingButtonEvent>;
}

/// @nodoc
class _$MultiFunctionFloatingButtonEventCopyWithImpl<$Res,
        $Val extends MultiFunctionFloatingButtonEvent>
    implements $MultiFunctionFloatingButtonEventCopyWith<$Res> {
  _$MultiFunctionFloatingButtonEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ToggleCopyWith<$Res> {
  factory _$$ToggleCopyWith(_$Toggle value, $Res Function(_$Toggle) then) =
      __$$ToggleCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToggleCopyWithImpl<$Res>
    extends _$MultiFunctionFloatingButtonEventCopyWithImpl<$Res, _$Toggle>
    implements _$$ToggleCopyWith<$Res> {
  __$$ToggleCopyWithImpl(_$Toggle _value, $Res Function(_$Toggle) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Toggle implements Toggle {
  const _$Toggle();

  @override
  String toString() {
    return 'MultiFunctionFloatingButtonEvent.toggle()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Toggle);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toggle,
  }) {
    return toggle();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? toggle,
  }) {
    return toggle?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toggle,
    required TResult orElse(),
  }) {
    if (toggle != null) {
      return toggle();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Toggle value) toggle,
  }) {
    return toggle(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Toggle value)? toggle,
  }) {
    return toggle?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Toggle value)? toggle,
    required TResult orElse(),
  }) {
    if (toggle != null) {
      return toggle(this);
    }
    return orElse();
  }
}

abstract class Toggle implements MultiFunctionFloatingButtonEvent {
  const factory Toggle() = _$Toggle;
}

/// @nodoc
mixin _$MultiFunctionFloatingButtonState {
  bool get open => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MultiFunctionFloatingButtonStateCopyWith<MultiFunctionFloatingButtonState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MultiFunctionFloatingButtonStateCopyWith<$Res> {
  factory $MultiFunctionFloatingButtonStateCopyWith(
          MultiFunctionFloatingButtonState value,
          $Res Function(MultiFunctionFloatingButtonState) then) =
      _$MultiFunctionFloatingButtonStateCopyWithImpl<$Res,
          MultiFunctionFloatingButtonState>;
  @useResult
  $Res call({bool open});
}

/// @nodoc
class _$MultiFunctionFloatingButtonStateCopyWithImpl<$Res,
        $Val extends MultiFunctionFloatingButtonState>
    implements $MultiFunctionFloatingButtonStateCopyWith<$Res> {
  _$MultiFunctionFloatingButtonStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? open = null,
  }) {
    return _then(_value.copyWith(
      open: null == open
          ? _value.open
          : open // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MultiFunctionFloatingButtonStateCopyWith<$Res>
    implements $MultiFunctionFloatingButtonStateCopyWith<$Res> {
  factory _$$_MultiFunctionFloatingButtonStateCopyWith(
          _$_MultiFunctionFloatingButtonState value,
          $Res Function(_$_MultiFunctionFloatingButtonState) then) =
      __$$_MultiFunctionFloatingButtonStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool open});
}

/// @nodoc
class __$$_MultiFunctionFloatingButtonStateCopyWithImpl<$Res>
    extends _$MultiFunctionFloatingButtonStateCopyWithImpl<$Res,
        _$_MultiFunctionFloatingButtonState>
    implements _$$_MultiFunctionFloatingButtonStateCopyWith<$Res> {
  __$$_MultiFunctionFloatingButtonStateCopyWithImpl(
      _$_MultiFunctionFloatingButtonState _value,
      $Res Function(_$_MultiFunctionFloatingButtonState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? open = null,
  }) {
    return _then(_$_MultiFunctionFloatingButtonState(
      open: null == open
          ? _value.open
          : open // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_MultiFunctionFloatingButtonState
    implements _MultiFunctionFloatingButtonState {
  const _$_MultiFunctionFloatingButtonState({required this.open});

  @override
  final bool open;

  @override
  String toString() {
    return 'MultiFunctionFloatingButtonState(open: $open)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MultiFunctionFloatingButtonState &&
            (identical(other.open, open) || other.open == open));
  }

  @override
  int get hashCode => Object.hash(runtimeType, open);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MultiFunctionFloatingButtonStateCopyWith<
          _$_MultiFunctionFloatingButtonState>
      get copyWith => __$$_MultiFunctionFloatingButtonStateCopyWithImpl<
          _$_MultiFunctionFloatingButtonState>(this, _$identity);
}

abstract class _MultiFunctionFloatingButtonState
    implements MultiFunctionFloatingButtonState {
  const factory _MultiFunctionFloatingButtonState({required final bool open}) =
      _$_MultiFunctionFloatingButtonState;

  @override
  bool get open;
  @override
  @JsonKey(ignore: true)
  _$$_MultiFunctionFloatingButtonStateCopyWith<
          _$_MultiFunctionFloatingButtonState>
      get copyWith => throw _privateConstructorUsedError;
}
