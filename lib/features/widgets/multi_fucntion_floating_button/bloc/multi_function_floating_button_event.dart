part of 'multi_function_floating_button_bloc.dart';

@freezed
class MultiFunctionFloatingButtonEvent with _$MultiFunctionFloatingButtonEvent {
  const factory MultiFunctionFloatingButtonEvent.toggle() = Toggle;
}
