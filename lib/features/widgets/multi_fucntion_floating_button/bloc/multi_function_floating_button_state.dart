part of 'multi_function_floating_button_bloc.dart';

@freezed
class MultiFunctionFloatingButtonState with _$MultiFunctionFloatingButtonState {
  const factory MultiFunctionFloatingButtonState({
    required bool open,
  }) = _MultiFunctionFloatingButtonState;

  factory MultiFunctionFloatingButtonState.initial() =>
      const MultiFunctionFloatingButtonState(
        open: false,
      );
}
