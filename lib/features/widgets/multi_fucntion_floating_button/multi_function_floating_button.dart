import 'package:acti_mentor/features/widgets/multi_fucntion_floating_button/bloc/multi_function_floating_button_bloc.dart';
import 'package:acti_mentor/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MultiFunctionFloatingButton extends StatelessWidget {
  final Icon? icon;
  final Icon? alternateIcon;
  final List<Widget> children;

  const MultiFunctionFloatingButton({
    Key? key,
    this.icon,
    this.alternateIcon,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MultiFunctionFloatingButtonBloc,
        MultiFunctionFloatingButtonState>(
      builder: (context, state) {
        if(state.open) {
          return FloatingActionButton(
            onPressed: () {
              getIt<MultiFunctionFloatingButtonBloc>()
                  .add(const MultiFunctionFloatingButtonEvent.toggle());
            },
            child: alternateIcon ?? const Icon(Icons.close),
          );
        } else {
          return FloatingActionButton(
            onPressed: () {
              getIt<MultiFunctionFloatingButtonBloc>()
                  .add(const MultiFunctionFloatingButtonEvent.toggle());
            },
            child: icon ?? const Icon(Icons.create),
          );
        }
      },
    );
  }
}
