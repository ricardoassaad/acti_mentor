import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:isar/isar.dart';

part 'category_model.g.dart';

@collection
class CategoryModel extends CategoryEntity {
  Id? id;
  final int? parentId;
  final String name;
  final String? color;

  CategoryModel({
    this.id,
    required this.parentId,
    required this.name,
    required this.color,
  }) : super(
          id: Isar.autoIncrement,
          parentId: parentId,
          name: name,
          color: color,
        );
}
