import 'package:acti_mentor/features/workouts/domain/entities/exercise_entity.dart';
import 'package:isar/isar.dart';

part 'exercise_model.g.dart';

@collection
class ExerciseModel extends ExerciseEntity {
  final Id id;
  final String name;
  final String? description;
  final String categoryId;

  ExerciseModel({
    required this.id,
    required this.name,
    required this.description,
    required this.categoryId,
  }) : super(
          id: Isar.autoIncrement,
          name: name,
          description: description,
          categoryId: categoryId,
        );
}
