import 'package:acti_mentor/errors/database_failure.dart';
import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/data/models/category_model.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:dartz/dartz.dart';

@Injectable(as: ICategoriesRepository)
class CategoriesRepository implements ICategoriesRepository {
  final _isar = getIt<Isar>();

  CategoriesRepository();

  @override
  Future<Either<Failure, CategoryEntity>> create(
      CategoryEntity categoryEntity) async {
    try {
      await _isar.writeTxn(() async {
        await _isar.categoryModels.put(CategoryModel(
          id: categoryEntity.id,
          parentId: categoryEntity.parentId,
          name: categoryEntity.name,
          color: categoryEntity.color,
        ));
      });

      return Right(categoryEntity);
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> delete(int id) async {
    try {
      CategoryModel? category =
          await _isar.categoryModels.where().idEqualTo(id).findFirst();
      if (category == null) {
        return const Left(DatabaseFailure.notFound());
      }

      bool result = await _isar.writeTxn(() async {
        return await _isar.categoryModels.delete(id);
      });

      if (result) {
        return const Right(true);
      } else {
        return const Left(DatabaseFailure.queryFailure());
      }
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, List<CategoryEntity>>> findAll() async {
    try {
      return Right(await _isar.categoryModels.where().findAll());
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, CategoryEntity>> read(int id) async {
    try {
      CategoryModel? category = await _isar.categoryModels.get(id);
      if (category != null) {
        return Right(category);
      } else {
        return const Left(DatabaseFailure.notFound());
      }
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, CategoryEntity>> update(
      CategoryEntity categoryEntity) async {
    try {
      CategoryModel? category = await _isar.categoryModels
          .where()
          .idEqualTo(categoryEntity.id!)
          .findFirst();
      if (category == null) {
        return const Left(DatabaseFailure.notFound());
      }
      await _isar.writeTxn(() async {
        return await _isar.categoryModels.put(CategoryModel(
          id: categoryEntity.id,
          parentId: categoryEntity.parentId ?? null,
          name: categoryEntity.name,
          color: categoryEntity.color ?? null,
        ));
      });

      return Right(categoryEntity);
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }
}
