import 'package:acti_mentor/errors/database_failure.dart';
import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/data/models/exercise_model.dart';
import 'package:acti_mentor/features/workouts/domain/entities/exercise_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_exercises_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';

@Injectable(as: IExercisesRepository)
class ExercisesRepository implements IExercisesRepository {
  final _isar = getIt<Isar>();

  ExercisesRepository();

  @override
  Future<Either<Failure, ExerciseEntity>> create(
      ExerciseEntity exerciseEntity) async {
    try {
      await _isar.writeTxn(() async {
        await _isar.exerciseModels.put(ExerciseModel(
          id: exerciseEntity.id,
          description: exerciseEntity.description,
          name: exerciseEntity.name,
          categoryId: exerciseEntity.categoryId,
        ));
      });

      return Right(exerciseEntity);
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> delete(int id) async {
    try {
      ExerciseModel? category =
          await _isar.exerciseModels.where().idEqualTo(id).findFirst();
      if (category == null) {
        return const Left(DatabaseFailure.notFound());
      }

      bool result = await _isar.writeTxn(() async {
        return await _isar.exerciseModels.delete(id);
      });

      if (result) {
        return const Right(true);
      } else {
        return const Left(DatabaseFailure.queryFailure());
      }
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, List<ExerciseEntity>>> findAll() async {
    try {
      return Right(await _isar.exerciseModels.where().findAll());
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, ExerciseEntity>> read(int id) async {
    try {
      ExerciseModel? category = await _isar.exerciseModels.get(id);
      if (category != null) {
        return Right(category);
      } else {
        return const Left(DatabaseFailure.notFound());
      }
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }

  @override
  Future<Either<Failure, ExerciseEntity>> update(
      ExerciseEntity exerciseEntity) async {
    try {
      ExerciseModel? category = await _isar.exerciseModels
          .where()
          .idEqualTo(exerciseEntity.id)
          .findFirst();
      if (category == null) {
        return const Left(DatabaseFailure.notFound());
      }

      await _isar.writeTxn(() async {
        await _isar.exerciseModels.put(ExerciseModel(
            id: exerciseEntity.id,
            description: exerciseEntity.description,
            name: exerciseEntity.name,
            categoryId: exerciseEntity.categoryId));
      });

      return Right(exerciseEntity);
    } on Exception {
      return const Left(DatabaseFailure.generalFailure());
    }
  }
}
