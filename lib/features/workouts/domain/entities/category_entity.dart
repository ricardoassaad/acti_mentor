class CategoryEntity {
  final int? id;
  //TODO: change this field from int to CategoryEntity
  final int? parentId;
  final String name;
  final String? color;

  CategoryEntity({
    required this.id,
    required this.parentId,
    required this.name,
    required this.color,
  });
}
