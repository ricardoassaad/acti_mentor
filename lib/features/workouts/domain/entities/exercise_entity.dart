class ExerciseEntity {
  final int id;
  final String name;
  //TODO: change this field from int to CategoryEntity
  final String categoryId;
  final String? description;

  ExerciseEntity({
    required this.id,
    required this.name,
    required this.categoryId,
    required this.description,
  });
}
