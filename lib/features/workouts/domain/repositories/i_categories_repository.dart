import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:dartz/dartz.dart';

abstract class ICategoriesRepository {
  Future<Either<Failure, List<CategoryEntity>>> findAll();

  Future<Either<Failure, CategoryEntity>> create(CategoryEntity categoryEntity);

  Future<Either<Failure, CategoryEntity>> read(int id);

  Future<Either<Failure, CategoryEntity>> update(CategoryEntity categoryEntity);

  Future<Either<Failure, bool>> delete(int id);
}
