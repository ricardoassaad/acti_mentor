import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/exercise_entity.dart';
import 'package:dartz/dartz.dart';

abstract class IExercisesRepository {
  Future<Either<Failure, List<ExerciseEntity>>> findAll();

  Future<Either<Failure, ExerciseEntity>> create(ExerciseEntity exerciseEntity);

  Future<Either<Failure, ExerciseEntity>> read(int id);

  Future<Either<Failure, ExerciseEntity>> update(ExerciseEntity exerciseEntity);

  Future<Either<Failure, bool>> delete(int id);
}
