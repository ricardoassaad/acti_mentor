import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class CreateCategoryUseCase {
  final _repository = getIt<ICategoriesRepository>();

  CreateCategoryUseCase();

  Future<Either<Failure, CategoryEntity>> call(
      String name, String? color, int? parentId) async {
    return await _repository.create(CategoryEntity(
      id: null,
      name: name,
      parentId: parentId,
      color: color,
    ));
  }
}
