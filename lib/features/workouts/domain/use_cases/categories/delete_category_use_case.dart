import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class DeleteCategoryUseCase {
  final _repository = getIt<ICategoriesRepository>();

  DeleteCategoryUseCase();

  Future<Either<Failure, bool>> call(int id) async {
    return await _repository.delete(id);
  }
}