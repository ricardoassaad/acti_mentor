import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class FetchAllCategoriesUseCase {
  final _repository = getIt<ICategoriesRepository>();

  FetchAllCategoriesUseCase();

  Future<Either<Failure, List<CategoryEntity>>> call() async {
    return await _repository.findAll();
  }
}