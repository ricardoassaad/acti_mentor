import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class UpdateCategoryUseCase {
  final _repository = getIt<ICategoriesRepository>();

  UpdateCategoryUseCase();

  Future<Either<Failure, CategoryEntity>> call(int id,
      String name, String? color, int? parentId) async {
    return await _repository.update(CategoryEntity(
      id: id,
      name: name,
      parentId: parentId,
      color: color,
    ));
  }
}
