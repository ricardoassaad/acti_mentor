import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/exercise_entity.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_exercises_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class CreateExerciseUseCase {
  final _repository = getIt<IExercisesRepository>();

  CreateExerciseUseCase();

  Future<Either<Failure, ExerciseEntity>> call(ExerciseEntity exerciseEntity) async {
    return await _repository.create(exerciseEntity);
  }
}