import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/repositories/i_exercises_repository.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';

class DeleteExerciseUseCase {
  final _repository = getIt<IExercisesRepository>();

  DeleteExerciseUseCase();

  Future<Either<Failure, bool>> call(int id) async {
    return await _repository.delete(id);
  }
}
