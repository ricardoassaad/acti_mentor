import 'package:acti_mentor/features/app_bar/presentation/bloc/app_bar_bloc.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/use_cases/categories/delete_category_use_case.dart';
import 'package:acti_mentor/features/workouts/domain/use_cases/categories/fetch_all_categories_use_case.dart';
import 'package:acti_mentor/injection.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'categories_event.dart';

part 'categories_state.dart';

part 'categories_bloc.freezed.dart';

@Singleton()
class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  CategoriesBloc() : super(CategoriesState.initial()) {
    on<_$Load>((event, emit) async {
      final categoriesOption = await FetchAllCategoriesUseCase().call();

      categoriesOption.fold((l) {
        emit(state.copyWith(errorMessage: l.toString()));
      }, (categories) {
        List<bool> items = [];
        for (int i = 0; i < categories.length; i++) {
          items.add(false);
        }
        emit(state.copyWith(categories: categories, selectedItems: items));
      });
    });
    on<_$Delete>((event, emit) async {
      List<String> failedToDelete = [];
      for (int i = 0; i < state.categories.length; i++) {
        if (state.selectedItems[i]){
          final result =
          await DeleteCategoryUseCase().call(state.categories[i].id!);
          result.fold(
                (l) {
              failedToDelete.add(l.toString());
            },
                (r) => null,
          );
        }
      }
      if (failedToDelete.isNotEmpty) {
        String errorMessage = "";
        for (int c = 0; c < failedToDelete.length; c++){
          String name = failedToDelete[c];
          errorMessage = "$errorMessage $name";
        }
        errorMessage = "$errorMessage failed to delete";
        emit(state.copyWith(errorMessage: errorMessage));
      }
      add(const CategoriesEvent.load());
      add(const CategoriesEvent.clearSelections());
    });
    on<_$ToggleItemSelection>((event, emit) async {
      List<bool> selectedItems = state.selectedItems.toList();
      selectedItems[event.index] = !selectedItems[event.index];
      emit(state.copyWith(selectedItems: selectedItems));
    });
    on<_$ClearSelections>((event, emit) async {
      List<bool> unselectedList = [];
      for (int i = 0; i < state.categories.length; i++) {
        unselectedList.add(false);
      }
      emit(state.copyWith(selectedItems: unselectedList));
      getIt<AppBarBloc>().add(const AppBarEvent.clear());
    });
  }
}
