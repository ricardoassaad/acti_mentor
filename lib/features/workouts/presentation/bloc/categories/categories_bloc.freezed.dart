// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'categories_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CategoriesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function() delete,
    required TResult Function(int index) toggleItemSelection,
    required TResult Function() clearSelections,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function()? delete,
    TResult? Function(int index)? toggleItemSelection,
    TResult? Function()? clearSelections,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function()? delete,
    TResult Function(int index)? toggleItemSelection,
    TResult Function()? clearSelections,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Delete value) delete,
    required TResult Function(ToggleItemSelection value) toggleItemSelection,
    required TResult Function(ClearSelections value) clearSelections,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Delete value)? delete,
    TResult? Function(ToggleItemSelection value)? toggleItemSelection,
    TResult? Function(ClearSelections value)? clearSelections,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Delete value)? delete,
    TResult Function(ToggleItemSelection value)? toggleItemSelection,
    TResult Function(ClearSelections value)? clearSelections,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoriesEventCopyWith<$Res> {
  factory $CategoriesEventCopyWith(
          CategoriesEvent value, $Res Function(CategoriesEvent) then) =
      _$CategoriesEventCopyWithImpl<$Res, CategoriesEvent>;
}

/// @nodoc
class _$CategoriesEventCopyWithImpl<$Res, $Val extends CategoriesEvent>
    implements $CategoriesEventCopyWith<$Res> {
  _$CategoriesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadCopyWith<$Res> {
  factory _$$LoadCopyWith(_$Load value, $Res Function(_$Load) then) =
      __$$LoadCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadCopyWithImpl<$Res>
    extends _$CategoriesEventCopyWithImpl<$Res, _$Load>
    implements _$$LoadCopyWith<$Res> {
  __$$LoadCopyWithImpl(_$Load _value, $Res Function(_$Load) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Load implements Load {
  const _$Load();

  @override
  String toString() {
    return 'CategoriesEvent.load()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Load);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function() delete,
    required TResult Function(int index) toggleItemSelection,
    required TResult Function() clearSelections,
  }) {
    return load();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function()? delete,
    TResult? Function(int index)? toggleItemSelection,
    TResult? Function()? clearSelections,
  }) {
    return load?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function()? delete,
    TResult Function(int index)? toggleItemSelection,
    TResult Function()? clearSelections,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Delete value) delete,
    required TResult Function(ToggleItemSelection value) toggleItemSelection,
    required TResult Function(ClearSelections value) clearSelections,
  }) {
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Delete value)? delete,
    TResult? Function(ToggleItemSelection value)? toggleItemSelection,
    TResult? Function(ClearSelections value)? clearSelections,
  }) {
    return load?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Delete value)? delete,
    TResult Function(ToggleItemSelection value)? toggleItemSelection,
    TResult Function(ClearSelections value)? clearSelections,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class Load implements CategoriesEvent {
  const factory Load() = _$Load;
}

/// @nodoc
abstract class _$$DeleteCopyWith<$Res> {
  factory _$$DeleteCopyWith(_$Delete value, $Res Function(_$Delete) then) =
      __$$DeleteCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeleteCopyWithImpl<$Res>
    extends _$CategoriesEventCopyWithImpl<$Res, _$Delete>
    implements _$$DeleteCopyWith<$Res> {
  __$$DeleteCopyWithImpl(_$Delete _value, $Res Function(_$Delete) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Delete implements Delete {
  const _$Delete();

  @override
  String toString() {
    return 'CategoriesEvent.delete()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Delete);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function() delete,
    required TResult Function(int index) toggleItemSelection,
    required TResult Function() clearSelections,
  }) {
    return delete();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function()? delete,
    TResult? Function(int index)? toggleItemSelection,
    TResult? Function()? clearSelections,
  }) {
    return delete?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function()? delete,
    TResult Function(int index)? toggleItemSelection,
    TResult Function()? clearSelections,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Delete value) delete,
    required TResult Function(ToggleItemSelection value) toggleItemSelection,
    required TResult Function(ClearSelections value) clearSelections,
  }) {
    return delete(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Delete value)? delete,
    TResult? Function(ToggleItemSelection value)? toggleItemSelection,
    TResult? Function(ClearSelections value)? clearSelections,
  }) {
    return delete?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Delete value)? delete,
    TResult Function(ToggleItemSelection value)? toggleItemSelection,
    TResult Function(ClearSelections value)? clearSelections,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete(this);
    }
    return orElse();
  }
}

abstract class Delete implements CategoriesEvent {
  const factory Delete() = _$Delete;
}

/// @nodoc
abstract class _$$ToggleItemSelectionCopyWith<$Res> {
  factory _$$ToggleItemSelectionCopyWith(_$ToggleItemSelection value,
          $Res Function(_$ToggleItemSelection) then) =
      __$$ToggleItemSelectionCopyWithImpl<$Res>;
  @useResult
  $Res call({int index});
}

/// @nodoc
class __$$ToggleItemSelectionCopyWithImpl<$Res>
    extends _$CategoriesEventCopyWithImpl<$Res, _$ToggleItemSelection>
    implements _$$ToggleItemSelectionCopyWith<$Res> {
  __$$ToggleItemSelectionCopyWithImpl(
      _$ToggleItemSelection _value, $Res Function(_$ToggleItemSelection) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? index = null,
  }) {
    return _then(_$ToggleItemSelection(
      null == index
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ToggleItemSelection implements ToggleItemSelection {
  const _$ToggleItemSelection(this.index);

  @override
  final int index;

  @override
  String toString() {
    return 'CategoriesEvent.toggleItemSelection(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToggleItemSelection &&
            (identical(other.index, index) || other.index == index));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ToggleItemSelectionCopyWith<_$ToggleItemSelection> get copyWith =>
      __$$ToggleItemSelectionCopyWithImpl<_$ToggleItemSelection>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function() delete,
    required TResult Function(int index) toggleItemSelection,
    required TResult Function() clearSelections,
  }) {
    return toggleItemSelection(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function()? delete,
    TResult? Function(int index)? toggleItemSelection,
    TResult? Function()? clearSelections,
  }) {
    return toggleItemSelection?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function()? delete,
    TResult Function(int index)? toggleItemSelection,
    TResult Function()? clearSelections,
    required TResult orElse(),
  }) {
    if (toggleItemSelection != null) {
      return toggleItemSelection(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Delete value) delete,
    required TResult Function(ToggleItemSelection value) toggleItemSelection,
    required TResult Function(ClearSelections value) clearSelections,
  }) {
    return toggleItemSelection(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Delete value)? delete,
    TResult? Function(ToggleItemSelection value)? toggleItemSelection,
    TResult? Function(ClearSelections value)? clearSelections,
  }) {
    return toggleItemSelection?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Delete value)? delete,
    TResult Function(ToggleItemSelection value)? toggleItemSelection,
    TResult Function(ClearSelections value)? clearSelections,
    required TResult orElse(),
  }) {
    if (toggleItemSelection != null) {
      return toggleItemSelection(this);
    }
    return orElse();
  }
}

abstract class ToggleItemSelection implements CategoriesEvent {
  const factory ToggleItemSelection(final int index) = _$ToggleItemSelection;

  int get index;
  @JsonKey(ignore: true)
  _$$ToggleItemSelectionCopyWith<_$ToggleItemSelection> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ClearSelectionsCopyWith<$Res> {
  factory _$$ClearSelectionsCopyWith(
          _$ClearSelections value, $Res Function(_$ClearSelections) then) =
      __$$ClearSelectionsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ClearSelectionsCopyWithImpl<$Res>
    extends _$CategoriesEventCopyWithImpl<$Res, _$ClearSelections>
    implements _$$ClearSelectionsCopyWith<$Res> {
  __$$ClearSelectionsCopyWithImpl(
      _$ClearSelections _value, $Res Function(_$ClearSelections) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ClearSelections implements ClearSelections {
  const _$ClearSelections();

  @override
  String toString() {
    return 'CategoriesEvent.clearSelections()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ClearSelections);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function() delete,
    required TResult Function(int index) toggleItemSelection,
    required TResult Function() clearSelections,
  }) {
    return clearSelections();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function()? delete,
    TResult? Function(int index)? toggleItemSelection,
    TResult? Function()? clearSelections,
  }) {
    return clearSelections?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function()? delete,
    TResult Function(int index)? toggleItemSelection,
    TResult Function()? clearSelections,
    required TResult orElse(),
  }) {
    if (clearSelections != null) {
      return clearSelections();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Delete value) delete,
    required TResult Function(ToggleItemSelection value) toggleItemSelection,
    required TResult Function(ClearSelections value) clearSelections,
  }) {
    return clearSelections(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Delete value)? delete,
    TResult? Function(ToggleItemSelection value)? toggleItemSelection,
    TResult? Function(ClearSelections value)? clearSelections,
  }) {
    return clearSelections?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Delete value)? delete,
    TResult Function(ToggleItemSelection value)? toggleItemSelection,
    TResult Function(ClearSelections value)? clearSelections,
    required TResult orElse(),
  }) {
    if (clearSelections != null) {
      return clearSelections(this);
    }
    return orElse();
  }
}

abstract class ClearSelections implements CategoriesEvent {
  const factory ClearSelections() = _$ClearSelections;
}

/// @nodoc
mixin _$CategoriesState {
  List<CategoryEntity> get categories => throw _privateConstructorUsedError;
  List<bool> get selectedItems => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CategoriesStateCopyWith<CategoriesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoriesStateCopyWith<$Res> {
  factory $CategoriesStateCopyWith(
          CategoriesState value, $Res Function(CategoriesState) then) =
      _$CategoriesStateCopyWithImpl<$Res, CategoriesState>;
  @useResult
  $Res call(
      {List<CategoryEntity> categories,
      List<bool> selectedItems,
      String? errorMessage});
}

/// @nodoc
class _$CategoriesStateCopyWithImpl<$Res, $Val extends CategoriesState>
    implements $CategoriesStateCopyWith<$Res> {
  _$CategoriesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categories = null,
    Object? selectedItems = null,
    Object? errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      categories: null == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<CategoryEntity>,
      selectedItems: null == selectedItems
          ? _value.selectedItems
          : selectedItems // ignore: cast_nullable_to_non_nullable
              as List<bool>,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CategoriesStateCopyWith<$Res>
    implements $CategoriesStateCopyWith<$Res> {
  factory _$$_CategoriesStateCopyWith(
          _$_CategoriesState value, $Res Function(_$_CategoriesState) then) =
      __$$_CategoriesStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<CategoryEntity> categories,
      List<bool> selectedItems,
      String? errorMessage});
}

/// @nodoc
class __$$_CategoriesStateCopyWithImpl<$Res>
    extends _$CategoriesStateCopyWithImpl<$Res, _$_CategoriesState>
    implements _$$_CategoriesStateCopyWith<$Res> {
  __$$_CategoriesStateCopyWithImpl(
      _$_CategoriesState _value, $Res Function(_$_CategoriesState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categories = null,
    Object? selectedItems = null,
    Object? errorMessage = freezed,
  }) {
    return _then(_$_CategoriesState(
      categories: null == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<CategoryEntity>,
      selectedItems: null == selectedItems
          ? _value._selectedItems
          : selectedItems // ignore: cast_nullable_to_non_nullable
              as List<bool>,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_CategoriesState implements _CategoriesState {
  const _$_CategoriesState(
      {required final List<CategoryEntity> categories,
      required final List<bool> selectedItems,
      this.errorMessage})
      : _categories = categories,
        _selectedItems = selectedItems;

  final List<CategoryEntity> _categories;
  @override
  List<CategoryEntity> get categories {
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_categories);
  }

  final List<bool> _selectedItems;
  @override
  List<bool> get selectedItems {
    if (_selectedItems is EqualUnmodifiableListView) return _selectedItems;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_selectedItems);
  }

  @override
  final String? errorMessage;

  @override
  String toString() {
    return 'CategoriesState(categories: $categories, selectedItems: $selectedItems, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CategoriesState &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories) &&
            const DeepCollectionEquality()
                .equals(other._selectedItems, _selectedItems) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_categories),
      const DeepCollectionEquality().hash(_selectedItems),
      errorMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CategoriesStateCopyWith<_$_CategoriesState> get copyWith =>
      __$$_CategoriesStateCopyWithImpl<_$_CategoriesState>(this, _$identity);
}

abstract class _CategoriesState implements CategoriesState {
  const factory _CategoriesState(
      {required final List<CategoryEntity> categories,
      required final List<bool> selectedItems,
      final String? errorMessage}) = _$_CategoriesState;

  @override
  List<CategoryEntity> get categories;
  @override
  List<bool> get selectedItems;
  @override
  String? get errorMessage;
  @override
  @JsonKey(ignore: true)
  _$$_CategoriesStateCopyWith<_$_CategoriesState> get copyWith =>
      throw _privateConstructorUsedError;
}
