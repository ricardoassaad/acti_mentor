part of 'categories_bloc.dart';

@freezed
class CategoriesEvent with _$CategoriesEvent {
  const factory CategoriesEvent.load() = Load;

  const factory CategoriesEvent.delete() = Delete;

  const factory CategoriesEvent.toggleItemSelection(int index) =
      ToggleItemSelection;
  const factory CategoriesEvent.clearSelections() = ClearSelections;
}
