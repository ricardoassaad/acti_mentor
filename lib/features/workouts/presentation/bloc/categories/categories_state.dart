part of 'categories_bloc.dart';

@freezed
class CategoriesState with _$CategoriesState {
  const factory CategoriesState({
    required List<CategoryEntity> categories,
    required List<bool> selectedItems,
    String? errorMessage,
  }) = _CategoriesState;

  factory CategoriesState.initial() => const CategoriesState(
        categories: [],
        errorMessage: null,
        selectedItems: [],
      );
}
