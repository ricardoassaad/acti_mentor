part of 'categories_add_form_bloc.dart';

@freezed
class CategoriesAddFormEvent with _$CategoriesAddFormEvent {
  const factory CategoriesAddFormEvent.initialize() = Initialize;
  const factory CategoriesAddFormEvent.nameChanged(String name) = NameChanged;
  const factory CategoriesAddFormEvent.parentIdChanged(int parentId) = ParentIdChanged;
  const factory CategoriesAddFormEvent.colorChanged(String? color) = ColorChanged;
  const factory CategoriesAddFormEvent.save() = Save;
  const factory CategoriesAddFormEvent.reset() = Reset;
}
