part of 'categories_add_form_bloc.dart';

@freezed
class CategoriesAddFormState with _$CategoriesAddFormState {
  const factory CategoriesAddFormState({
    required List? categories,
    required String? errorMessage,
    required String? name,
    required int? parentId,
    required String? color,
    required bool saving,
    required bool saved,
    required Option<Either<DatabaseFailure, Unit>> saveFailureOrSuccessOption,
  }) = _CategoriesAddFormState;

  factory CategoriesAddFormState.initial() => CategoriesAddFormState(
        categories: [],
        name: null,
        color: null,
        parentId: null,
        errorMessage: null,
        saving: false,
        saved: false,
        saveFailureOrSuccessOption: none(),
      );
}
