import 'package:acti_mentor/errors/database_failure.dart';
import 'package:acti_mentor/errors/failures.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/domain/use_cases/categories/fetch_all_categories_use_case.dart';
import 'package:acti_mentor/features/workouts/domain/use_cases/categories/update_category_use_case.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories/categories_bloc.dart';
import 'package:acti_mentor/injection.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'categories_edit_form_event.dart';

part 'categories_edit_form_state.dart';

part 'categories_edit_form_bloc.freezed.dart';

@Singleton()
class CategoriesEditFormBloc
    extends Bloc<CategoriesEditFormEvent, CategoriesEditFormState> {
  CategoriesEditFormBloc() : super(CategoriesEditFormState.initial()) {
    on<_$Initialize>((event, emit) async {
      emit(state.copyWith(
        id: event.category.id!,
        name: event.category.name,
        parentId: event.category.parentId,
        color: event.category.color,
      ));
      Either<Failure, List<CategoryEntity>> categoriesOrFailure =
          await FetchAllCategoriesUseCase().call();
      categoriesOrFailure.fold(
        (l) {
          emit(state.copyWith(errorMessage: l.toString()));
        },
        (r) => emit(state.copyWith(categories: r)),
      );
    });
    on<_$NameChanged>((event, emit) async {
      emit(
        state.copyWith(
          name: event.name,
        ),
      );
    });
    on<_$ParentIdChanged>((event, emit) async {
      emit(
        state.copyWith(
          parentId: event.parentId,
        ),
      );
    });
    on<_$ColorChanged>((event, emit) async {
      emit(
        state.copyWith(
          color: event.color,
        ),
      );
    });
    on<_$Save>((event, emit) async {
      Either<Failure, CategoryEntity> failureOrSuccess =
          await UpdateCategoryUseCase().call(
        state.id,
        state.name!,
        state.color,
        state.parentId,
      );
      failureOrSuccess.fold(
        (failure) => null,
        (category) {
          emit(
            state.copyWith(saved: true),
          );
          //update the categories state to display the latest added category
          getIt<CategoriesBloc>().add(const CategoriesEvent.load());
          add(CategoriesEditFormEvent.reset(category));
        },
      );
    });
    on<_$Reset>((event, emit) async {
      emit(CategoriesEditFormState.initial());
      add(CategoriesEditFormEvent.initialize(event.category));
    });
  }
}
