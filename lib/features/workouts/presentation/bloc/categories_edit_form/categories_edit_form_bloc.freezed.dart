// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'categories_edit_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$CategoriesEditFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoriesEditFormEventCopyWith<$Res> {
  factory $CategoriesEditFormEventCopyWith(CategoriesEditFormEvent value,
          $Res Function(CategoriesEditFormEvent) then) =
      _$CategoriesEditFormEventCopyWithImpl<$Res, CategoriesEditFormEvent>;
}

/// @nodoc
class _$CategoriesEditFormEventCopyWithImpl<$Res,
        $Val extends CategoriesEditFormEvent>
    implements $CategoriesEditFormEventCopyWith<$Res> {
  _$CategoriesEditFormEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitializeCopyWith<$Res> {
  factory _$$InitializeCopyWith(
          _$Initialize value, $Res Function(_$Initialize) then) =
      __$$InitializeCopyWithImpl<$Res>;
  @useResult
  $Res call({CategoryEntity category});
}

/// @nodoc
class __$$InitializeCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$Initialize>
    implements _$$InitializeCopyWith<$Res> {
  __$$InitializeCopyWithImpl(
      _$Initialize _value, $Res Function(_$Initialize) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? category = null,
  }) {
    return _then(_$Initialize(
      null == category
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as CategoryEntity,
    ));
  }
}

/// @nodoc

class _$Initialize implements Initialize {
  const _$Initialize(this.category);

  @override
  final CategoryEntity category;

  @override
  String toString() {
    return 'CategoriesEditFormEvent.initialize(category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Initialize &&
            (identical(other.category, category) ||
                other.category == category));
  }

  @override
  int get hashCode => Object.hash(runtimeType, category);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InitializeCopyWith<_$Initialize> get copyWith =>
      __$$InitializeCopyWithImpl<_$Initialize>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return initialize(category);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return initialize?.call(category);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (initialize != null) {
      return initialize(category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return initialize(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return initialize?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (initialize != null) {
      return initialize(this);
    }
    return orElse();
  }
}

abstract class Initialize implements CategoriesEditFormEvent {
  const factory Initialize(final CategoryEntity category) = _$Initialize;

  CategoryEntity get category;
  @JsonKey(ignore: true)
  _$$InitializeCopyWith<_$Initialize> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NameChangedCopyWith<$Res> {
  factory _$$NameChangedCopyWith(
          _$NameChanged value, $Res Function(_$NameChanged) then) =
      __$$NameChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String name});
}

/// @nodoc
class __$$NameChangedCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$NameChanged>
    implements _$$NameChangedCopyWith<$Res> {
  __$$NameChangedCopyWithImpl(
      _$NameChanged _value, $Res Function(_$NameChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
  }) {
    return _then(_$NameChanged(
      null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NameChanged implements NameChanged {
  const _$NameChanged(this.name);

  @override
  final String name;

  @override
  String toString() {
    return 'CategoriesEditFormEvent.nameChanged(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NameChanged &&
            (identical(other.name, name) || other.name == name));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NameChangedCopyWith<_$NameChanged> get copyWith =>
      __$$NameChangedCopyWithImpl<_$NameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return nameChanged(name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return nameChanged?.call(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return nameChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class NameChanged implements CategoriesEditFormEvent {
  const factory NameChanged(final String name) = _$NameChanged;

  String get name;
  @JsonKey(ignore: true)
  _$$NameChangedCopyWith<_$NameChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ParentIdChangedCopyWith<$Res> {
  factory _$$ParentIdChangedCopyWith(
          _$ParentIdChanged value, $Res Function(_$ParentIdChanged) then) =
      __$$ParentIdChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({int parentId});
}

/// @nodoc
class __$$ParentIdChangedCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$ParentIdChanged>
    implements _$$ParentIdChangedCopyWith<$Res> {
  __$$ParentIdChangedCopyWithImpl(
      _$ParentIdChanged _value, $Res Function(_$ParentIdChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? parentId = null,
  }) {
    return _then(_$ParentIdChanged(
      null == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ParentIdChanged implements ParentIdChanged {
  const _$ParentIdChanged(this.parentId);

  @override
  final int parentId;

  @override
  String toString() {
    return 'CategoriesEditFormEvent.parentIdChanged(parentId: $parentId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ParentIdChanged &&
            (identical(other.parentId, parentId) ||
                other.parentId == parentId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, parentId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ParentIdChangedCopyWith<_$ParentIdChanged> get copyWith =>
      __$$ParentIdChangedCopyWithImpl<_$ParentIdChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return parentIdChanged(parentId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return parentIdChanged?.call(parentId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (parentIdChanged != null) {
      return parentIdChanged(parentId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return parentIdChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return parentIdChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (parentIdChanged != null) {
      return parentIdChanged(this);
    }
    return orElse();
  }
}

abstract class ParentIdChanged implements CategoriesEditFormEvent {
  const factory ParentIdChanged(final int parentId) = _$ParentIdChanged;

  int get parentId;
  @JsonKey(ignore: true)
  _$$ParentIdChangedCopyWith<_$ParentIdChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ColorChangedCopyWith<$Res> {
  factory _$$ColorChangedCopyWith(
          _$ColorChanged value, $Res Function(_$ColorChanged) then) =
      __$$ColorChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String? color});
}

/// @nodoc
class __$$ColorChangedCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$ColorChanged>
    implements _$$ColorChangedCopyWith<$Res> {
  __$$ColorChangedCopyWithImpl(
      _$ColorChanged _value, $Res Function(_$ColorChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? color = freezed,
  }) {
    return _then(_$ColorChanged(
      freezed == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$ColorChanged implements ColorChanged {
  const _$ColorChanged(this.color);

  @override
  final String? color;

  @override
  String toString() {
    return 'CategoriesEditFormEvent.colorChanged(color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ColorChanged &&
            (identical(other.color, color) || other.color == color));
  }

  @override
  int get hashCode => Object.hash(runtimeType, color);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ColorChangedCopyWith<_$ColorChanged> get copyWith =>
      __$$ColorChangedCopyWithImpl<_$ColorChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return colorChanged(color);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return colorChanged?.call(color);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (colorChanged != null) {
      return colorChanged(color);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return colorChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return colorChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (colorChanged != null) {
      return colorChanged(this);
    }
    return orElse();
  }
}

abstract class ColorChanged implements CategoriesEditFormEvent {
  const factory ColorChanged(final String? color) = _$ColorChanged;

  String? get color;
  @JsonKey(ignore: true)
  _$$ColorChangedCopyWith<_$ColorChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SaveCopyWith<$Res> {
  factory _$$SaveCopyWith(_$Save value, $Res Function(_$Save) then) =
      __$$SaveCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SaveCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$Save>
    implements _$$SaveCopyWith<$Res> {
  __$$SaveCopyWithImpl(_$Save _value, $Res Function(_$Save) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Save implements Save {
  const _$Save();

  @override
  String toString() {
    return 'CategoriesEditFormEvent.save()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Save);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return save();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return save?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (save != null) {
      return save();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return save(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return save?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (save != null) {
      return save(this);
    }
    return orElse();
  }
}

abstract class Save implements CategoriesEditFormEvent {
  const factory Save() = _$Save;
}

/// @nodoc
abstract class _$$ResetCopyWith<$Res> {
  factory _$$ResetCopyWith(_$Reset value, $Res Function(_$Reset) then) =
      __$$ResetCopyWithImpl<$Res>;
  @useResult
  $Res call({CategoryEntity category});
}

/// @nodoc
class __$$ResetCopyWithImpl<$Res>
    extends _$CategoriesEditFormEventCopyWithImpl<$Res, _$Reset>
    implements _$$ResetCopyWith<$Res> {
  __$$ResetCopyWithImpl(_$Reset _value, $Res Function(_$Reset) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? category = null,
  }) {
    return _then(_$Reset(
      null == category
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as CategoryEntity,
    ));
  }
}

/// @nodoc

class _$Reset implements Reset {
  const _$Reset(this.category);

  @override
  final CategoryEntity category;

  @override
  String toString() {
    return 'CategoriesEditFormEvent.reset(category: $category)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Reset &&
            (identical(other.category, category) ||
                other.category == category));
  }

  @override
  int get hashCode => Object.hash(runtimeType, category);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ResetCopyWith<_$Reset> get copyWith =>
      __$$ResetCopyWithImpl<_$Reset>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CategoryEntity category) initialize,
    required TResult Function(String name) nameChanged,
    required TResult Function(int parentId) parentIdChanged,
    required TResult Function(String? color) colorChanged,
    required TResult Function() save,
    required TResult Function(CategoryEntity category) reset,
  }) {
    return reset(category);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(CategoryEntity category)? initialize,
    TResult? Function(String name)? nameChanged,
    TResult? Function(int parentId)? parentIdChanged,
    TResult? Function(String? color)? colorChanged,
    TResult? Function()? save,
    TResult? Function(CategoryEntity category)? reset,
  }) {
    return reset?.call(category);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CategoryEntity category)? initialize,
    TResult Function(String name)? nameChanged,
    TResult Function(int parentId)? parentIdChanged,
    TResult Function(String? color)? colorChanged,
    TResult Function()? save,
    TResult Function(CategoryEntity category)? reset,
    required TResult orElse(),
  }) {
    if (reset != null) {
      return reset(category);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initialize value) initialize,
    required TResult Function(NameChanged value) nameChanged,
    required TResult Function(ParentIdChanged value) parentIdChanged,
    required TResult Function(ColorChanged value) colorChanged,
    required TResult Function(Save value) save,
    required TResult Function(Reset value) reset,
  }) {
    return reset(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Initialize value)? initialize,
    TResult? Function(NameChanged value)? nameChanged,
    TResult? Function(ParentIdChanged value)? parentIdChanged,
    TResult? Function(ColorChanged value)? colorChanged,
    TResult? Function(Save value)? save,
    TResult? Function(Reset value)? reset,
  }) {
    return reset?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initialize value)? initialize,
    TResult Function(NameChanged value)? nameChanged,
    TResult Function(ParentIdChanged value)? parentIdChanged,
    TResult Function(ColorChanged value)? colorChanged,
    TResult Function(Save value)? save,
    TResult Function(Reset value)? reset,
    required TResult orElse(),
  }) {
    if (reset != null) {
      return reset(this);
    }
    return orElse();
  }
}

abstract class Reset implements CategoriesEditFormEvent {
  const factory Reset(final CategoryEntity category) = _$Reset;

  CategoryEntity get category;
  @JsonKey(ignore: true)
  _$$ResetCopyWith<_$Reset> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$CategoriesEditFormState {
  List<dynamic>? get categories => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  int? get parentId => throw _privateConstructorUsedError;
  String? get color => throw _privateConstructorUsedError;
  bool get saving => throw _privateConstructorUsedError;
  bool get saved => throw _privateConstructorUsedError;
  Option<Either<DatabaseFailure, Unit>> get saveFailureOrSuccessOption =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CategoriesEditFormStateCopyWith<CategoriesEditFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategoriesEditFormStateCopyWith<$Res> {
  factory $CategoriesEditFormStateCopyWith(CategoriesEditFormState value,
          $Res Function(CategoriesEditFormState) then) =
      _$CategoriesEditFormStateCopyWithImpl<$Res, CategoriesEditFormState>;
  @useResult
  $Res call(
      {List<dynamic>? categories,
      int id,
      String? errorMessage,
      String? name,
      int? parentId,
      String? color,
      bool saving,
      bool saved,
      Option<Either<DatabaseFailure, Unit>> saveFailureOrSuccessOption});
}

/// @nodoc
class _$CategoriesEditFormStateCopyWithImpl<$Res,
        $Val extends CategoriesEditFormState>
    implements $CategoriesEditFormStateCopyWith<$Res> {
  _$CategoriesEditFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categories = freezed,
    Object? id = null,
    Object? errorMessage = freezed,
    Object? name = freezed,
    Object? parentId = freezed,
    Object? color = freezed,
    Object? saving = null,
    Object? saved = null,
    Object? saveFailureOrSuccessOption = null,
  }) {
    return _then(_value.copyWith(
      categories: freezed == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      parentId: freezed == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as int?,
      color: freezed == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      saving: null == saving
          ? _value.saving
          : saving // ignore: cast_nullable_to_non_nullable
              as bool,
      saved: null == saved
          ? _value.saved
          : saved // ignore: cast_nullable_to_non_nullable
              as bool,
      saveFailureOrSuccessOption: null == saveFailureOrSuccessOption
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<DatabaseFailure, Unit>>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CategoriesEditFormStateCopyWith<$Res>
    implements $CategoriesEditFormStateCopyWith<$Res> {
  factory _$$_CategoriesEditFormStateCopyWith(_$_CategoriesEditFormState value,
          $Res Function(_$_CategoriesEditFormState) then) =
      __$$_CategoriesEditFormStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<dynamic>? categories,
      int id,
      String? errorMessage,
      String? name,
      int? parentId,
      String? color,
      bool saving,
      bool saved,
      Option<Either<DatabaseFailure, Unit>> saveFailureOrSuccessOption});
}

/// @nodoc
class __$$_CategoriesEditFormStateCopyWithImpl<$Res>
    extends _$CategoriesEditFormStateCopyWithImpl<$Res,
        _$_CategoriesEditFormState>
    implements _$$_CategoriesEditFormStateCopyWith<$Res> {
  __$$_CategoriesEditFormStateCopyWithImpl(_$_CategoriesEditFormState _value,
      $Res Function(_$_CategoriesEditFormState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? categories = freezed,
    Object? id = null,
    Object? errorMessage = freezed,
    Object? name = freezed,
    Object? parentId = freezed,
    Object? color = freezed,
    Object? saving = null,
    Object? saved = null,
    Object? saveFailureOrSuccessOption = null,
  }) {
    return _then(_$_CategoriesEditFormState(
      categories: freezed == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<dynamic>?,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      parentId: freezed == parentId
          ? _value.parentId
          : parentId // ignore: cast_nullable_to_non_nullable
              as int?,
      color: freezed == color
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String?,
      saving: null == saving
          ? _value.saving
          : saving // ignore: cast_nullable_to_non_nullable
              as bool,
      saved: null == saved
          ? _value.saved
          : saved // ignore: cast_nullable_to_non_nullable
              as bool,
      saveFailureOrSuccessOption: null == saveFailureOrSuccessOption
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<DatabaseFailure, Unit>>,
    ));
  }
}

/// @nodoc

class _$_CategoriesEditFormState implements _CategoriesEditFormState {
  const _$_CategoriesEditFormState(
      {required final List<dynamic>? categories,
      required this.id,
      required this.errorMessage,
      required this.name,
      required this.parentId,
      required this.color,
      required this.saving,
      required this.saved,
      required this.saveFailureOrSuccessOption})
      : _categories = categories;

  final List<dynamic>? _categories;
  @override
  List<dynamic>? get categories {
    final value = _categories;
    if (value == null) return null;
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final int id;
  @override
  final String? errorMessage;
  @override
  final String? name;
  @override
  final int? parentId;
  @override
  final String? color;
  @override
  final bool saving;
  @override
  final bool saved;
  @override
  final Option<Either<DatabaseFailure, Unit>> saveFailureOrSuccessOption;

  @override
  String toString() {
    return 'CategoriesEditFormState(categories: $categories, id: $id, errorMessage: $errorMessage, name: $name, parentId: $parentId, color: $color, saving: $saving, saved: $saved, saveFailureOrSuccessOption: $saveFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CategoriesEditFormState &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.parentId, parentId) ||
                other.parentId == parentId) &&
            (identical(other.color, color) || other.color == color) &&
            (identical(other.saving, saving) || other.saving == saving) &&
            (identical(other.saved, saved) || other.saved == saved) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                other.saveFailureOrSuccessOption ==
                    saveFailureOrSuccessOption));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_categories),
      id,
      errorMessage,
      name,
      parentId,
      color,
      saving,
      saved,
      saveFailureOrSuccessOption);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CategoriesEditFormStateCopyWith<_$_CategoriesEditFormState>
      get copyWith =>
          __$$_CategoriesEditFormStateCopyWithImpl<_$_CategoriesEditFormState>(
              this, _$identity);
}

abstract class _CategoriesEditFormState implements CategoriesEditFormState {
  const factory _CategoriesEditFormState(
      {required final List<dynamic>? categories,
      required final int id,
      required final String? errorMessage,
      required final String? name,
      required final int? parentId,
      required final String? color,
      required final bool saving,
      required final bool saved,
      required final Option<Either<DatabaseFailure, Unit>>
          saveFailureOrSuccessOption}) = _$_CategoriesEditFormState;

  @override
  List<dynamic>? get categories;
  @override
  int get id;
  @override
  String? get errorMessage;
  @override
  String? get name;
  @override
  int? get parentId;
  @override
  String? get color;
  @override
  bool get saving;
  @override
  bool get saved;
  @override
  Option<Either<DatabaseFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  @JsonKey(ignore: true)
  _$$_CategoriesEditFormStateCopyWith<_$_CategoriesEditFormState>
      get copyWith => throw _privateConstructorUsedError;
}
