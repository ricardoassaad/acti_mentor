part of 'categories_edit_form_bloc.dart';

@freezed
class CategoriesEditFormEvent with _$CategoriesEditFormEvent {
  const factory CategoriesEditFormEvent.initialize(CategoryEntity category) =
      Initialize;

  const factory CategoriesEditFormEvent.nameChanged(String name) = NameChanged;

  const factory CategoriesEditFormEvent.parentIdChanged(int parentId) =
      ParentIdChanged;

  const factory CategoriesEditFormEvent.colorChanged(String? color) =
      ColorChanged;

  const factory CategoriesEditFormEvent.save() = Save;
  const factory CategoriesEditFormEvent.reset(CategoryEntity category) = Reset;
}
