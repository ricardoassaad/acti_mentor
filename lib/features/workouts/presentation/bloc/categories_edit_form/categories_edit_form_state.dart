part of 'categories_edit_form_bloc.dart';

@freezed
class CategoriesEditFormState with _$CategoriesEditFormState {
  const factory CategoriesEditFormState({
    required List? categories,
    required int id,
    required String? errorMessage,
    required String? name,
    required int? parentId,
    required String? color,
    required bool saving,
    required bool saved,
    required Option<Either<DatabaseFailure, Unit>> saveFailureOrSuccessOption,
  }) = _CategoriesEditFormState;

  factory CategoriesEditFormState.initial() => CategoriesEditFormState(
        categories: [],
        id:0,
        name: null,
        color: null,
        parentId: null,
        errorMessage: null,
        saving: false,
        saved: false,
        saveFailureOrSuccessOption: none(),
      );
}
