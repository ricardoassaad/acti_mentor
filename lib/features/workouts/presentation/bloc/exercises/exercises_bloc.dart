import 'package:acti_mentor/features/workouts/domain/entities/exercise_entity.dart';
import 'package:acti_mentor/features/workouts/domain/use_cases/exercises/fetch_all_exercises_use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'exercises_event.dart';

part 'exercises_state.dart';

part 'exercises_bloc.freezed.dart';

@Singleton()
class ExercisesBloc extends Bloc<ExercisesEvent, ExercisesState> {
  ExercisesBloc()
      : super(ExercisesState.initial()) {
    on<_$Load>((event, emit) async {
      final exercisesOption =
          await FetchAllExercisesUseCase().call();

      emit(
        state.copyWith(
            errorMessage: exercisesOption.fold((l) {
          print("left");
          return "error";
        }, (r) {
          print("right");
          return "not error";
        })),
      );
    });
    on<_$Create>((event, emit) async {
      // emit(
      //   state.copyWith(
      //     themeModeSetting: ThemesService().theme,
      //   ),
      // );
    });
    on<_$Read>((event, emit) async {
      // emit(
      //   state.copyWith(
      //     themeModeSetting: ThemesService().theme,
      //   ),
      // );
    });
    on<_$Update>((event, emit) async {
      // emit(
      //   state.copyWith(
      //     themeModeSetting: ThemesService().theme,
      //   ),
      // );
    });
    on<_$Delete>((event, emit) async {
      // emit(
      //   state.copyWith(
      //     themeModeSetting: ThemesService().theme,
      //   ),
      // );
    });
  }
}
