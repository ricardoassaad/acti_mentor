// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'exercises_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ExercisesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExercisesEventCopyWith<$Res> {
  factory $ExercisesEventCopyWith(
          ExercisesEvent value, $Res Function(ExercisesEvent) then) =
      _$ExercisesEventCopyWithImpl<$Res, ExercisesEvent>;
}

/// @nodoc
class _$ExercisesEventCopyWithImpl<$Res, $Val extends ExercisesEvent>
    implements $ExercisesEventCopyWith<$Res> {
  _$ExercisesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadCopyWith<$Res> {
  factory _$$LoadCopyWith(_$Load value, $Res Function(_$Load) then) =
      __$$LoadCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadCopyWithImpl<$Res>
    extends _$ExercisesEventCopyWithImpl<$Res, _$Load>
    implements _$$LoadCopyWith<$Res> {
  __$$LoadCopyWithImpl(_$Load _value, $Res Function(_$Load) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Load implements Load {
  const _$Load();

  @override
  String toString() {
    return 'ExercisesEvent.load()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Load);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) {
    return load();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) {
    return load?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) {
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) {
    return load?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) {
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class Load implements ExercisesEvent {
  const factory Load() = _$Load;
}

/// @nodoc
abstract class _$$CreateCopyWith<$Res> {
  factory _$$CreateCopyWith(_$Create value, $Res Function(_$Create) then) =
      __$$CreateCopyWithImpl<$Res>;
  @useResult
  $Res call({ExerciseEntity exerciseEntity});
}

/// @nodoc
class __$$CreateCopyWithImpl<$Res>
    extends _$ExercisesEventCopyWithImpl<$Res, _$Create>
    implements _$$CreateCopyWith<$Res> {
  __$$CreateCopyWithImpl(_$Create _value, $Res Function(_$Create) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exerciseEntity = null,
  }) {
    return _then(_$Create(
      null == exerciseEntity
          ? _value.exerciseEntity
          : exerciseEntity // ignore: cast_nullable_to_non_nullable
              as ExerciseEntity,
    ));
  }
}

/// @nodoc

class _$Create implements Create {
  const _$Create(this.exerciseEntity);

  @override
  final ExerciseEntity exerciseEntity;

  @override
  String toString() {
    return 'ExercisesEvent.create(exerciseEntity: $exerciseEntity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Create &&
            (identical(other.exerciseEntity, exerciseEntity) ||
                other.exerciseEntity == exerciseEntity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, exerciseEntity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateCopyWith<_$Create> get copyWith =>
      __$$CreateCopyWithImpl<_$Create>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) {
    return create(exerciseEntity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) {
    return create?.call(exerciseEntity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) {
    if (create != null) {
      return create(exerciseEntity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) {
    return create(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) {
    return create?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) {
    if (create != null) {
      return create(this);
    }
    return orElse();
  }
}

abstract class Create implements ExercisesEvent {
  const factory Create(final ExerciseEntity exerciseEntity) = _$Create;

  ExerciseEntity get exerciseEntity;
  @JsonKey(ignore: true)
  _$$CreateCopyWith<_$Create> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ReadCopyWith<$Res> {
  factory _$$ReadCopyWith(_$Read value, $Res Function(_$Read) then) =
      __$$ReadCopyWithImpl<$Res>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$ReadCopyWithImpl<$Res>
    extends _$ExercisesEventCopyWithImpl<$Res, _$Read>
    implements _$$ReadCopyWith<$Res> {
  __$$ReadCopyWithImpl(_$Read _value, $Res Function(_$Read) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$Read(
      null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Read implements Read {
  const _$Read(this.id);

  @override
  final int id;

  @override
  String toString() {
    return 'ExercisesEvent.read(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Read &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadCopyWith<_$Read> get copyWith =>
      __$$ReadCopyWithImpl<_$Read>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) {
    return read(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) {
    return read?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) {
    return read(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) {
    return read?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(this);
    }
    return orElse();
  }
}

abstract class Read implements ExercisesEvent {
  const factory Read(final int id) = _$Read;

  int get id;
  @JsonKey(ignore: true)
  _$$ReadCopyWith<_$Read> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UpdateCopyWith<$Res> {
  factory _$$UpdateCopyWith(_$Update value, $Res Function(_$Update) then) =
      __$$UpdateCopyWithImpl<$Res>;
  @useResult
  $Res call({ExerciseEntity exerciseEntity});
}

/// @nodoc
class __$$UpdateCopyWithImpl<$Res>
    extends _$ExercisesEventCopyWithImpl<$Res, _$Update>
    implements _$$UpdateCopyWith<$Res> {
  __$$UpdateCopyWithImpl(_$Update _value, $Res Function(_$Update) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exerciseEntity = null,
  }) {
    return _then(_$Update(
      null == exerciseEntity
          ? _value.exerciseEntity
          : exerciseEntity // ignore: cast_nullable_to_non_nullable
              as ExerciseEntity,
    ));
  }
}

/// @nodoc

class _$Update implements Update {
  const _$Update(this.exerciseEntity);

  @override
  final ExerciseEntity exerciseEntity;

  @override
  String toString() {
    return 'ExercisesEvent.update(exerciseEntity: $exerciseEntity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Update &&
            (identical(other.exerciseEntity, exerciseEntity) ||
                other.exerciseEntity == exerciseEntity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, exerciseEntity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateCopyWith<_$Update> get copyWith =>
      __$$UpdateCopyWithImpl<_$Update>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) {
    return update(exerciseEntity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) {
    return update?.call(exerciseEntity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update(exerciseEntity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) {
    return update(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) {
    return update?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update(this);
    }
    return orElse();
  }
}

abstract class Update implements ExercisesEvent {
  const factory Update(final ExerciseEntity exerciseEntity) = _$Update;

  ExerciseEntity get exerciseEntity;
  @JsonKey(ignore: true)
  _$$UpdateCopyWith<_$Update> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DeleteCopyWith<$Res> {
  factory _$$DeleteCopyWith(_$Delete value, $Res Function(_$Delete) then) =
      __$$DeleteCopyWithImpl<$Res>;
  @useResult
  $Res call({int id});
}

/// @nodoc
class __$$DeleteCopyWithImpl<$Res>
    extends _$ExercisesEventCopyWithImpl<$Res, _$Delete>
    implements _$$DeleteCopyWith<$Res> {
  __$$DeleteCopyWithImpl(_$Delete _value, $Res Function(_$Delete) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
  }) {
    return _then(_$Delete(
      null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Delete implements Delete {
  const _$Delete(this.id);

  @override
  final int id;

  @override
  String toString() {
    return 'ExercisesEvent.delete(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Delete &&
            (identical(other.id, id) || other.id == id));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DeleteCopyWith<_$Delete> get copyWith =>
      __$$DeleteCopyWithImpl<_$Delete>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() load,
    required TResult Function(ExerciseEntity exerciseEntity) create,
    required TResult Function(int id) read,
    required TResult Function(ExerciseEntity exerciseEntity) update,
    required TResult Function(int id) delete,
  }) {
    return delete(id);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? load,
    TResult? Function(ExerciseEntity exerciseEntity)? create,
    TResult? Function(int id)? read,
    TResult? Function(ExerciseEntity exerciseEntity)? update,
    TResult? Function(int id)? delete,
  }) {
    return delete?.call(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? load,
    TResult Function(ExerciseEntity exerciseEntity)? create,
    TResult Function(int id)? read,
    TResult Function(ExerciseEntity exerciseEntity)? update,
    TResult Function(int id)? delete,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Load value) load,
    required TResult Function(Create value) create,
    required TResult Function(Read value) read,
    required TResult Function(Update value) update,
    required TResult Function(Delete value) delete,
  }) {
    return delete(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Load value)? load,
    TResult? Function(Create value)? create,
    TResult? Function(Read value)? read,
    TResult? Function(Update value)? update,
    TResult? Function(Delete value)? delete,
  }) {
    return delete?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Load value)? load,
    TResult Function(Create value)? create,
    TResult Function(Read value)? read,
    TResult Function(Update value)? update,
    TResult Function(Delete value)? delete,
    required TResult orElse(),
  }) {
    if (delete != null) {
      return delete(this);
    }
    return orElse();
  }
}

abstract class Delete implements ExercisesEvent {
  const factory Delete(final int id) = _$Delete;

  int get id;
  @JsonKey(ignore: true)
  _$$DeleteCopyWith<_$Delete> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ExercisesState {
  List<ExerciseEntity> get exercises => throw _privateConstructorUsedError;
  ExerciseEntity? get currentExercise => throw _privateConstructorUsedError;
  String? get errorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExercisesStateCopyWith<ExercisesState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExercisesStateCopyWith<$Res> {
  factory $ExercisesStateCopyWith(
          ExercisesState value, $Res Function(ExercisesState) then) =
      _$ExercisesStateCopyWithImpl<$Res, ExercisesState>;
  @useResult
  $Res call(
      {List<ExerciseEntity> exercises,
      ExerciseEntity? currentExercise,
      String? errorMessage});
}

/// @nodoc
class _$ExercisesStateCopyWithImpl<$Res, $Val extends ExercisesState>
    implements $ExercisesStateCopyWith<$Res> {
  _$ExercisesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exercises = null,
    Object? currentExercise = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_value.copyWith(
      exercises: null == exercises
          ? _value.exercises
          : exercises // ignore: cast_nullable_to_non_nullable
              as List<ExerciseEntity>,
      currentExercise: freezed == currentExercise
          ? _value.currentExercise
          : currentExercise // ignore: cast_nullable_to_non_nullable
              as ExerciseEntity?,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ExercisesStateCopyWith<$Res>
    implements $ExercisesStateCopyWith<$Res> {
  factory _$$_ExercisesStateCopyWith(
          _$_ExercisesState value, $Res Function(_$_ExercisesState) then) =
      __$$_ExercisesStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ExerciseEntity> exercises,
      ExerciseEntity? currentExercise,
      String? errorMessage});
}

/// @nodoc
class __$$_ExercisesStateCopyWithImpl<$Res>
    extends _$ExercisesStateCopyWithImpl<$Res, _$_ExercisesState>
    implements _$$_ExercisesStateCopyWith<$Res> {
  __$$_ExercisesStateCopyWithImpl(
      _$_ExercisesState _value, $Res Function(_$_ExercisesState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exercises = null,
    Object? currentExercise = freezed,
    Object? errorMessage = freezed,
  }) {
    return _then(_$_ExercisesState(
      exercises: null == exercises
          ? _value._exercises
          : exercises // ignore: cast_nullable_to_non_nullable
              as List<ExerciseEntity>,
      currentExercise: freezed == currentExercise
          ? _value.currentExercise
          : currentExercise // ignore: cast_nullable_to_non_nullable
              as ExerciseEntity?,
      errorMessage: freezed == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ExercisesState implements _ExercisesState {
  const _$_ExercisesState(
      {required final List<ExerciseEntity> exercises,
      this.currentExercise,
      this.errorMessage})
      : _exercises = exercises;

  final List<ExerciseEntity> _exercises;
  @override
  List<ExerciseEntity> get exercises {
    if (_exercises is EqualUnmodifiableListView) return _exercises;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_exercises);
  }

  @override
  final ExerciseEntity? currentExercise;
  @override
  final String? errorMessage;

  @override
  String toString() {
    return 'ExercisesState(exercises: $exercises, currentExercise: $currentExercise, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ExercisesState &&
            const DeepCollectionEquality()
                .equals(other._exercises, _exercises) &&
            (identical(other.currentExercise, currentExercise) ||
                other.currentExercise == currentExercise) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_exercises),
      currentExercise,
      errorMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ExercisesStateCopyWith<_$_ExercisesState> get copyWith =>
      __$$_ExercisesStateCopyWithImpl<_$_ExercisesState>(this, _$identity);
}

abstract class _ExercisesState implements ExercisesState {
  const factory _ExercisesState(
      {required final List<ExerciseEntity> exercises,
      final ExerciseEntity? currentExercise,
      final String? errorMessage}) = _$_ExercisesState;

  @override
  List<ExerciseEntity> get exercises;
  @override
  ExerciseEntity? get currentExercise;
  @override
  String? get errorMessage;
  @override
  @JsonKey(ignore: true)
  _$$_ExercisesStateCopyWith<_$_ExercisesState> get copyWith =>
      throw _privateConstructorUsedError;
}
