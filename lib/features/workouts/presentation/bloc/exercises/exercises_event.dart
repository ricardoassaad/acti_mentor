part of 'exercises_bloc.dart';

@freezed
class ExercisesEvent with _$ExercisesEvent {
  const factory ExercisesEvent.load() = Load;

  const factory ExercisesEvent.create(ExerciseEntity exerciseEntity) = Create;

  const factory ExercisesEvent.read(int id) = Read;

  const factory ExercisesEvent.update(ExerciseEntity exerciseEntity) = Update;

  const factory ExercisesEvent.delete(int id) = Delete;
}
