part of 'exercises_bloc.dart';

@freezed
class ExercisesState with _$ExercisesState {
  const factory ExercisesState({
    required List<ExerciseEntity> exercises,
    ExerciseEntity? currentExercise,
    String? errorMessage,
  }) = _ExercisesState;

  factory ExercisesState.initial() => const ExercisesState(
        exercises: [],
        currentExercise: null,
        errorMessage: null,
      );
}
