import 'package:acti_mentor/features/workouts/presentation/bloc/categories_add_form/categories_add_form_bloc.dart';
import 'package:acti_mentor/theme/constants.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

@RoutePage<bool>()
class CategoriesAddScreen extends StatelessWidget {
  const CategoriesAddScreen({Key? key}) : super(key: key);

  List<DropdownMenuItem>? populateDropdownItems(List? categories) {
    if (categories == null) {
      return null;
    }
    return categories
        .map((category) => DropdownMenuItem(
              value: category.id,
              child: Text(category.name),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CategoriesAddFormBloc, CategoriesAddFormState>(
        listener: (context, state) {
      if (state.saved) {
        context.router.pop();
      }
    }, builder: (context, state) {
      return WillPopScope(
        onWillPop: () async {
          context
              .read<CategoriesAddFormBloc>()
              .add(const CategoriesAddFormEvent.reset());
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Add New Category'),
          ),
          body: Form(
            child: ListView(
              padding: const EdgeInsets.all(20),
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  autofocus: true,
                  initialValue: state.name,
                  onChanged: (value) => context
                      .read<CategoriesAddFormBloc>()
                      .add(CategoriesAddFormEvent.nameChanged(value)),
                ),
                const SizedBox(height: 30),
                DropdownButtonFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Parent Category',
                  ),
                  value: state.parentId,
                  items: populateDropdownItems(state.categories),
                  onChanged: (value) => context
                      .read<CategoriesAddFormBloc>()
                      .add(CategoriesAddFormEvent.parentIdChanged(value)),
                ),
                const SizedBox(height: 30),
                Row(children: [
                  if (state.color != null) ...[
                    Expanded(
                        flex: 1,
                        child: CircleAvatar(
                            backgroundColor: Color(int.parse(state.color!)))),
                  ],
                  Expanded(
                    flex: 4,
                    child: ElevatedButton(
                      onPressed: () {
                        context.read<CategoriesAddFormBloc>().add(
                            CategoriesAddFormEvent.colorChanged(
                                Constants().primary.value.toString()));
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                titlePadding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                contentPadding: const EdgeInsets.only(
                                  left: 0.0,
                                  right: 0.0,
                                ),
                                title:
                                    const Center(child: Text("Pick a color")),
                                content: SingleChildScrollView(
                                  child: ColorPicker(
                                    // TODO: pick a color from the theme
                                    pickerColor: Constants().primary,
                                    onColorChanged: (color) {
                                      context.read<CategoriesAddFormBloc>().add(
                                          CategoriesAddFormEvent.colorChanged(
                                              color.value.toString()));
                                    },
                                    colorPickerWidth: 300.0,
                                    pickerAreaHeightPercent: 0.7,
                                    enableAlpha: true,
                                    displayThumbColor: true,
                                    paletteType: PaletteType.hsv,
                                  ),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      context.read<CategoriesAddFormBloc>().add(
                                            const CategoriesAddFormEvent
                                                .colorChanged(null),
                                          );
                                      Navigator.pop(context, 'Cancel');
                                    },
                                    child: const Text('Cancel'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      context.read<CategoriesAddFormBloc>().add(
                                            CategoriesAddFormEvent.colorChanged(
                                                context
                                                    .read<
                                                        CategoriesAddFormBloc>()
                                                    .state
                                                    .color),
                                          );
                                      Navigator.pop(context, 'OK');
                                    },
                                    child: const Text('OK'),
                                  ),
                                ],
                              );
                            });
                      },
                      child: const Text('Pick a Color'),
                    ),
                  ),
                ]),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              context
                  .read<CategoriesAddFormBloc>()
                  .add(const CategoriesAddFormEvent.save());
            },
            child: const Icon(Icons.check),
          ),
        ),
      );
    });
  }
}
