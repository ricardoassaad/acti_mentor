import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_edit_form/categories_edit_form_bloc.dart';
import 'package:acti_mentor/theme/constants.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

@RoutePage<bool>()
class CategoriesEditScreen extends StatelessWidget {
  final CategoryEntity category;

  const CategoriesEditScreen({Key? key, required this.category})
      : super(key: key);

  List<DropdownMenuItem>? populateDropdownItems(List? categories) {
    if (categories == null) {
      return null;
    }
    return categories
        .map((category) => DropdownMenuItem(
              value: category.id,
              child: Text(category.name),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CategoriesEditFormBloc, CategoriesEditFormState>(
        listener: (context, state) {
      if (state.saved) {
        context.router.pop();
      }
    }, builder: (context, state) {
      return WillPopScope(
        onWillPop: () async {
          context
              .read<CategoriesEditFormBloc>()
              .add(CategoriesEditFormEvent.reset(category));
          return true;
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Edit Category'),
          ),
          body: Form(
            child: ListView(
              padding: const EdgeInsets.all(20),
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  initialValue: state.name,
                  onChanged: (value) => context
                      .read<CategoriesEditFormBloc>()
                      .add(CategoriesEditFormEvent.nameChanged(value)),
                ),
                const SizedBox(height: 30),
                DropdownButtonFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Parent Category',
                  ),
                  value: state.parentId,
                  items: populateDropdownItems(state.categories),
                  onChanged: (value) =>
                      context.read<CategoriesEditFormBloc>().add(
                            CategoriesEditFormEvent.parentIdChanged(value),
                          ),
                ),
                const SizedBox(height: 30),
                Row(children: [
                  if (state.color != null) ...[
                    Expanded(
                        flex: 1,
                        child: CircleAvatar(
                            backgroundColor: Color(int.parse(state.color!)))),
                  ],
                  Expanded(
                    flex: 4,
                    child: ElevatedButton(
                      onPressed: () {
                        context.read<CategoriesEditFormBloc>().add(
                            CategoriesEditFormEvent.colorChanged(
                                Constants().primary.value.toString()));
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                titlePadding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                contentPadding: const EdgeInsets.only(
                                  left: 0.0,
                                  right: 0.0,
                                ),
                                title:
                                    const Center(child: Text("Pick a color")),
                                content: SingleChildScrollView(
                                  child: ColorPicker(
                                    // TODO: pick a color from the theme
                                    pickerColor: (state.color == null)
                                        ? Constants().primary
                                        : Color(int.parse(state.color!)),
                                    onColorChanged: (color) {
                                      context
                                          .read<CategoriesEditFormBloc>()
                                          .add(CategoriesEditFormEvent
                                              .colorChanged(
                                                  color.value.toString()));
                                    },
                                    colorPickerWidth: 300.0,
                                    pickerAreaHeightPercent: 0.7,
                                    enableAlpha: true,
                                    displayThumbColor: true,
                                    paletteType: PaletteType.hsv,
                                  ),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      context
                                          .read<CategoriesEditFormBloc>()
                                          .add(
                                            const CategoriesEditFormEvent
                                                .colorChanged(null),
                                          );
                                      Navigator.pop(context, 'Cancel');
                                    },
                                    child: const Text('Cancel'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      context
                                          .read<CategoriesEditFormBloc>()
                                          .add(
                                            CategoriesEditFormEvent
                                                .colorChanged(context
                                                    .read<
                                                        CategoriesEditFormBloc>()
                                                    .state
                                                    .color),
                                          );
                                      Navigator.pop(context, 'OK');
                                    },
                                    child: const Text('OK'),
                                  ),
                                ],
                              );
                            });
                      },
                      child: const Text('Pick a Color'),
                    ),
                  ),
                ]),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              context
                  .read<CategoriesEditFormBloc>()
                  .add(const CategoriesEditFormEvent.save());
            },
            child: const Icon(Icons.check),
          ),
        ),
      );
    });
  }
}
