import 'package:acti_mentor/features/app_bar/presentation/bloc/app_bar_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories/categories_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_edit_form/categories_edit_form_bloc.dart';
import 'package:acti_mentor/injection.dart';
import 'package:acti_mentor/routes/router.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage<bool>()
class CategoriesIndexScreen extends StatelessWidget {
  const CategoriesIndexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CategoriesBloc, CategoriesState>(
      listener: (context, state) {
        if (state.selectedItems.contains(true)) {
          getIt<AppBarBloc>().add(AppBarEvent.push([
            IconButton(
                onPressed: () {
                  context
                      .read<CategoriesBloc>()
                      .add(const CategoriesEvent.delete());
                },
                icon: const Icon(Icons.delete)),
            IconButton(
                onPressed: () {
                  getIt<AppBarBloc>().add(const AppBarEvent.clear());
                  context
                      .read<CategoriesBloc>()
                      .add(const CategoriesEvent.clearSelections());
                },
                icon: const Icon(Icons.close))
          ]));
        } else {
          getIt<AppBarBloc>().add(const AppBarEvent.clear());
        }
      },
      builder: (context, state) {
        if (state.categories.isEmpty) {
          return const Center(
            child: Text('you have no categories yet'),
          );
        }
        return ListView.separated(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
          ),
          itemCount: state.categories.length,
          itemBuilder: (context, index) {
            return ListTile(
              selected: state.selectedItems[index],
              onLongPress: () {
                context
                    .read<CategoriesBloc>()
                    .add(CategoriesEvent.toggleItemSelection(index));
              },
              onTap: () {
                if (state.selectedItems.contains(true)) {
                  context
                      .read<CategoriesBloc>()
                      .add(CategoriesEvent.toggleItemSelection(index));
                } else {
                  getIt<CategoriesEditFormBloc>().add(
                    CategoriesEditFormEvent.initialize(state.categories[index]),
                  );
                  context.router.push(
                    CategoriesEditRoute(category: state.categories[index]),
                  );
                }
              },
              title: Text(state.categories[index].name),
              leading: CircleAvatar(
                backgroundColor: Color(
                  int.parse(state.categories[index].color!),
                ),
              ),
            );
          },
          separatorBuilder: (context, index) => const Divider(),
        );
      },
    );
  }
}
