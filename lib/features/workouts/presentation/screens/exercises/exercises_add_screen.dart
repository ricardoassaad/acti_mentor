import 'package:acti_mentor/features/workouts/presentation/bloc/categories_add_form/categories_add_form_bloc.dart';
import 'package:acti_mentor/injection.dart';
import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage<bool>()
class ExercisesAddScreen extends StatelessWidget {
  const ExercisesAddScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => getIt<CategoriesAddFormBloc>(),
      child: const Scaffold(
        body: Center(child: Text("exercises add")),
      ),
    );
  }
}
