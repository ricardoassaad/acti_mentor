import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>()
class ExercisesEditScreen extends StatelessWidget {
  const ExercisesEditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: Text("exercises")),
    );
  }
}
