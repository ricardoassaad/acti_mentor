import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>()
class RoutinesIndexScreen extends StatelessWidget {
  const RoutinesIndexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Text("routine"));
  }
}
