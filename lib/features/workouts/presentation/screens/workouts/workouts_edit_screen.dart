import 'package:acti_mentor/features/workouts/presentation/widgets/workout_card_widget.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>()
class WorkoutsEditScreen extends StatelessWidget {
  const WorkoutsEditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: ListView(
        children: [
          Row(
            children: const [
              Text("May 2023"),
            ],
          ),
          Column(
            children: [
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
              Row(
                children: const [
                  WorkoutCardWidget(),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
