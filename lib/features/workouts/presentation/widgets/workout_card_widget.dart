import 'package:flutter/material.dart';

class WorkoutCardWidget extends StatelessWidget {
  const WorkoutCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Card(
        margin: const EdgeInsets.all(8),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Row(
                children: const [
                  Expanded(child: Text("title")),
                ],
              ),
              Row(
                children: const [
                  Expanded(child: Text(textAlign: TextAlign.right, "2 x 10")),
                ],
              ),
              Row(
                children: const [
                  Expanded(child: Text(textAlign: TextAlign.right, "2 x 10")),
                ],
              ),
              Row(
                children: const [
                  Expanded(child: Text(textAlign: TextAlign.right, "2 x 10")),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
