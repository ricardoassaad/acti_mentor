import 'package:acti_mentor/routes/router.dart';
import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

@RoutePage<bool>(
  name: 'TabBarWidgetRouter',
)
class TabBarWidgetScreen extends StatelessWidget {
  const TabBarWidgetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter.tabBar(
      routes: const [
        WorkoutsIndexRoute(),
        RoutinesIndexRoute(),
        ExercisesIndexRoute(),
        CategoriesIndexRoute(),
      ],
      builder: (context, child, tabController) {
        TabsRouter tabsRouter = AutoTabsRouter.of(context);
        return Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: NavigationBar(
                selectedIndex: tabController.index,
                onDestinationSelected: (index) {
                  tabsRouter.setActiveIndex(index);
                },
                destinations: const [
                  NavigationDestination(
                    //TODO: make the text internationalized
                    label: "Workouts",
                    icon: Icon(Icons.auto_graph),
                  ),
                  NavigationDestination(
                    //TODO: make the text internationalized
                    label: "Routines",
                    icon: Icon(Icons.replay_outlined),
                  ),
                  NavigationDestination(
                    //TODO: make the text internationalized
                    label: "Exercises",
                    icon: Icon(Icons.accessibility_new),
                  ),
                  NavigationDestination(
                    //TODO: make the text internationalized
                    label: "Categories",
                    icon: Icon(Icons.category),
                  ),
                ]),
          ),
          body: child,
        );
      },
    );
  }
}
