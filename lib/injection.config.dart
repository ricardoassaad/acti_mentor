// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:acti_mentor/features/app_bar/presentation/bloc/app_bar_bloc.dart'
    as _i3;
import 'package:acti_mentor/features/dashboard/presentation/bloc/dashboard_bloc.dart'
    as _i7;
import 'package:acti_mentor/features/widgets/multi_fucntion_floating_button/bloc/multi_function_floating_button_bloc.dart'
    as _i15;
import 'package:acti_mentor/features/workouts/data/repositories/categories_repository.dart'
    as _i11;
import 'package:acti_mentor/features/workouts/data/repositories/exercises_repository.dart'
    as _i13;
import 'package:acti_mentor/features/workouts/domain/repositories/i_categories_repository.dart'
    as _i10;
import 'package:acti_mentor/features/workouts/domain/repositories/i_exercises_repository.dart'
    as _i12;
import 'package:acti_mentor/features/workouts/presentation/bloc/categories/categories_bloc.dart'
    as _i5;
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_add_form/categories_add_form_bloc.dart'
    as _i4;
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_edit_form/categories_edit_form_bloc.dart'
    as _i6;
import 'package:acti_mentor/features/workouts/presentation/bloc/exercises/exercises_bloc.dart'
    as _i8;
import 'package:acti_mentor/service_providers/get_storage_injectable_module.dart'
    as _i17;
import 'package:acti_mentor/service_providers/isar_injectable_module.dart'
    as _i18;
import 'package:acti_mentor/theme/bloc/theme_bloc.dart' as _i16;
import 'package:get_it/get_it.dart' as _i1;
import 'package:get_storage/get_storage.dart' as _i9;
import 'package:injectable/injectable.dart' as _i2;
import 'package:isar/isar.dart' as _i14;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final getStorageInjectableModule = _$GetStorageInjectableModule();
  final isarInjectableModule = _$IsarInjectableModule();
  gh.singleton<_i3.AppBarBloc>(_i3.AppBarBloc());
  gh.singleton<_i4.CategoriesAddFormBloc>(_i4.CategoriesAddFormBloc());
  gh.singleton<_i5.CategoriesBloc>(_i5.CategoriesBloc());
  gh.singleton<_i6.CategoriesEditFormBloc>(_i6.CategoriesEditFormBloc());
  gh.singleton<_i7.DashBoardBloc>(_i7.DashBoardBloc());
  gh.singleton<_i8.ExercisesBloc>(_i8.ExercisesBloc());
  gh.singleton<_i9.GetStorage>(getStorageInjectableModule.getStorage);
  gh.factory<_i10.ICategoriesRepository>(() => _i11.CategoriesRepository());
  gh.factory<_i12.IExercisesRepository>(() => _i13.ExercisesRepository());
  gh.singletonAsync<_i14.Isar>(() => isarInjectableModule.isar);
  gh.singleton<_i15.MultiFunctionFloatingButtonBloc>(
      _i15.MultiFunctionFloatingButtonBloc());
  gh.singleton<_i16.ThemeBloc>(_i16.ThemeBloc());
  return getIt;
}

class _$GetStorageInjectableModule extends _i17.GetStorageInjectableModule {}

class _$IsarInjectableModule extends _i18.IsarInjectableModule {}
