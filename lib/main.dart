import 'package:acti_mentor/features/app_bar/presentation/bloc/app_bar_bloc.dart';
import 'package:acti_mentor/features/dashboard/presentation/bloc/dashboard_bloc.dart';
import 'package:acti_mentor/features/widgets/multi_fucntion_floating_button/bloc/multi_function_floating_button_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_add_form/categories_add_form_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories_edit_form/categories_edit_form_bloc.dart';
import 'package:acti_mentor/theme/themes_service.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/categories/categories_bloc.dart';
import 'package:acti_mentor/features/workouts/presentation/bloc/exercises/exercises_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:acti_mentor/routes/router.dart';
import 'package:acti_mentor/theme/themes.dart';
import 'package:acti_mentor/theme/bloc/theme_bloc.dart';

import 'injection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  await configureInjection(Environment.prod);

  getIt.registerSingleton<AppRouter>(AppRouter());

  runApp(App());
}

class App extends StatelessWidget {
  App({super.key});

  final _appRouter = AppRouter();
  final ThemeMode _themeMode = ThemesService().theme;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ThemeBloc>(
          create: (context) =>
              getIt<ThemeBloc>()..add(const ThemeEvent.update()),
        ),
        BlocProvider<AppBarBloc>(
          create: (context) => getIt<AppBarBloc>(),
        ),
        BlocProvider<DashBoardBloc>(
          create: (context) => getIt<DashBoardBloc>(),
        ),
        BlocProvider<CategoriesBloc>(
          create: (context) =>
              getIt<CategoriesBloc>()..add(const CategoriesEvent.load()),
        ),
        BlocProvider<MultiFunctionFloatingButtonBloc>(
          create: (context) => getIt<MultiFunctionFloatingButtonBloc>(),
        ),
        BlocProvider<CategoriesAddFormBloc>(
          create: (context) => getIt<CategoriesAddFormBloc>(),
        ),
        BlocProvider<CategoriesEditFormBloc>(
          create: (context) => getIt<CategoriesEditFormBloc>(),
        ),
        BlocProvider<ExercisesBloc>(
          create: (context) => getIt<ExercisesBloc>(),
        ),
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routerDelegate: _appRouter.delegate(),
        routeInformationParser: _appRouter.defaultRouteParser(),
        title: 'Acti Mentor',
        theme: Themes.light,
        darkTheme: Themes.dark,
        // TODO: ThemesService().theme should be saved in state
        themeMode: _themeMode,
      ),
    );
  }
}
