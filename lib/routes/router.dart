import 'package:acti_mentor/features/app_widget.dart';
import 'package:acti_mentor/features/dashboard/presentation/screens/dashboard_screen.dart';
import 'package:acti_mentor/features/tracker/presentation/screens/tracker_home_screen.dart';
import 'package:acti_mentor/features/workouts/domain/entities/category_entity.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/categories/categories_add_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/categories/categories_edit_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/categories/categories_index_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/exercises/exercises_add_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/exercises/exercises_edit_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/exercises/exercises_index_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/routines/routines_add_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/routines/routines_edit_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/routines/routines_index_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/workouts/workouts_add_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/workouts/workouts_edit_screen.dart';
import 'package:acti_mentor/features/workouts/presentation/screens/workouts/workouts_index_screen.dart';
import 'package:acti_mentor/features/workouts/tab_bar_widget.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

part 'router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
            initial: true,
            path: '/',
            page: AppRootRouter.page,
            children: [
              AutoRoute(
                path: '',
                page: DashBoardRoute.page,
              ),
              AutoRoute(
                path: 'tabs',
                page: TabBarWidgetRouter.page,
                children: [
                  AutoRoute(
                    path: 'workouts',
                    page: WorkoutsIndexRoute.page,
                  ),
                  AutoRoute(
                    path: 'exercises',
                    page: ExercisesIndexRoute.page,
                  ),
                  AutoRoute(
                    path: 'routines',
                    page: RoutinesIndexRoute.page,
                  ),
                  AutoRoute(
                    path: 'categories',
                    page: CategoriesIndexRoute.page,
                  ),
                ],
              ),
              AutoRoute(
                path: 'trackers',
                page: TrackerHomeRoute.page,
              ),
            ]),

        AutoRoute(
          path: '/workouts/add',
          page: WorkoutsAddRoute.page,
        ),
        AutoRoute(
          path: '/workouts/edit',
          page: WorkoutsEditRoute.page,
        ),
        AutoRoute(
          path: '/categories/add',
          page: CategoriesAddRoute.page,
        ),
        AutoRoute(
          path: '/categories/edit',
          page: CategoriesEditRoute.page,
        ),
        AutoRoute(
          path: '/routines/add',
          page: RoutinesAddRoute.page,
        ),
        AutoRoute(
          path: '/routines/edit',
          page: RoutinesEditRoute.page,
        ),
        AutoRoute(
          path: '/exercises/add',
          page: ExercisesAddRoute.page,
        ),
        AutoRoute(
          path: '/exercises/edit',
          page: ExercisesEditRoute.page,
        ),
      ];
}
