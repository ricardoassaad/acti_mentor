// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    AppRootRouter.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const AppWidgetScreen(),
      );
    },
    DashBoardRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const DashBoardScreen(),
      );
    },
    TrackerHomeRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const TrackerHomeScreen(),
      );
    },
    CategoriesAddRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const CategoriesAddScreen(),
      );
    },
    CategoriesEditRoute.name: (routeData) {
      final args = routeData.argsAs<CategoriesEditRouteArgs>();
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: CategoriesEditScreen(
          key: args.key,
          category: args.category,
        ),
      );
    },
    CategoriesIndexRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const CategoriesIndexScreen(),
      );
    },
    ExercisesAddRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const ExercisesAddScreen(),
      );
    },
    ExercisesEditRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const ExercisesEditScreen(),
      );
    },
    ExercisesIndexRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const ExercisesIndexScreen(),
      );
    },
    RoutinesAddRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const RoutinesAddScreen(),
      );
    },
    RoutinesEditRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const RoutinesEditScreen(),
      );
    },
    RoutinesIndexRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const RoutinesIndexScreen(),
      );
    },
    WorkoutsAddRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const WorkoutsAddScreen(),
      );
    },
    WorkoutsEditRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const WorkoutsEditScreen(),
      );
    },
    WorkoutsIndexRoute.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const WorkoutsIndexScreen(),
      );
    },
    TabBarWidgetRouter.name: (routeData) {
      return AutoRoutePage<bool>(
        routeData: routeData,
        child: const TabBarWidgetScreen(),
      );
    },
  };
}

/// generated route for
/// [AppWidgetScreen]
class AppRootRouter extends PageRouteInfo<void> {
  const AppRootRouter({List<PageRouteInfo>? children})
      : super(
          AppRootRouter.name,
          initialChildren: children,
        );

  static const String name = 'AppRootRouter';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [DashBoardScreen]
class DashBoardRoute extends PageRouteInfo<void> {
  const DashBoardRoute({List<PageRouteInfo>? children})
      : super(
          DashBoardRoute.name,
          initialChildren: children,
        );

  static const String name = 'DashBoardRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [TrackerHomeScreen]
class TrackerHomeRoute extends PageRouteInfo<void> {
  const TrackerHomeRoute({List<PageRouteInfo>? children})
      : super(
          TrackerHomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'TrackerHomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [CategoriesAddScreen]
class CategoriesAddRoute extends PageRouteInfo<void> {
  const CategoriesAddRoute({List<PageRouteInfo>? children})
      : super(
          CategoriesAddRoute.name,
          initialChildren: children,
        );

  static const String name = 'CategoriesAddRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [CategoriesEditScreen]
class CategoriesEditRoute extends PageRouteInfo<CategoriesEditRouteArgs> {
  CategoriesEditRoute({
    Key? key,
    required CategoryEntity category,
    List<PageRouteInfo>? children,
  }) : super(
          CategoriesEditRoute.name,
          args: CategoriesEditRouteArgs(
            key: key,
            category: category,
          ),
          initialChildren: children,
        );

  static const String name = 'CategoriesEditRoute';

  static const PageInfo<CategoriesEditRouteArgs> page =
      PageInfo<CategoriesEditRouteArgs>(name);
}

class CategoriesEditRouteArgs {
  const CategoriesEditRouteArgs({
    this.key,
    required this.category,
  });

  final Key? key;

  final CategoryEntity category;

  @override
  String toString() {
    return 'CategoriesEditRouteArgs{key: $key, category: $category}';
  }
}

/// generated route for
/// [CategoriesIndexScreen]
class CategoriesIndexRoute extends PageRouteInfo<void> {
  const CategoriesIndexRoute({List<PageRouteInfo>? children})
      : super(
          CategoriesIndexRoute.name,
          initialChildren: children,
        );

  static const String name = 'CategoriesIndexRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ExercisesAddScreen]
class ExercisesAddRoute extends PageRouteInfo<void> {
  const ExercisesAddRoute({List<PageRouteInfo>? children})
      : super(
          ExercisesAddRoute.name,
          initialChildren: children,
        );

  static const String name = 'ExercisesAddRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ExercisesEditScreen]
class ExercisesEditRoute extends PageRouteInfo<void> {
  const ExercisesEditRoute({List<PageRouteInfo>? children})
      : super(
          ExercisesEditRoute.name,
          initialChildren: children,
        );

  static const String name = 'ExercisesEditRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ExercisesIndexScreen]
class ExercisesIndexRoute extends PageRouteInfo<void> {
  const ExercisesIndexRoute({List<PageRouteInfo>? children})
      : super(
          ExercisesIndexRoute.name,
          initialChildren: children,
        );

  static const String name = 'ExercisesIndexRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RoutinesAddScreen]
class RoutinesAddRoute extends PageRouteInfo<void> {
  const RoutinesAddRoute({List<PageRouteInfo>? children})
      : super(
          RoutinesAddRoute.name,
          initialChildren: children,
        );

  static const String name = 'RoutinesAddRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RoutinesEditScreen]
class RoutinesEditRoute extends PageRouteInfo<void> {
  const RoutinesEditRoute({List<PageRouteInfo>? children})
      : super(
          RoutinesEditRoute.name,
          initialChildren: children,
        );

  static const String name = 'RoutinesEditRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [RoutinesIndexScreen]
class RoutinesIndexRoute extends PageRouteInfo<void> {
  const RoutinesIndexRoute({List<PageRouteInfo>? children})
      : super(
          RoutinesIndexRoute.name,
          initialChildren: children,
        );

  static const String name = 'RoutinesIndexRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [WorkoutsAddScreen]
class WorkoutsAddRoute extends PageRouteInfo<void> {
  const WorkoutsAddRoute({List<PageRouteInfo>? children})
      : super(
          WorkoutsAddRoute.name,
          initialChildren: children,
        );

  static const String name = 'WorkoutsAddRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [WorkoutsEditScreen]
class WorkoutsEditRoute extends PageRouteInfo<void> {
  const WorkoutsEditRoute({List<PageRouteInfo>? children})
      : super(
          WorkoutsEditRoute.name,
          initialChildren: children,
        );

  static const String name = 'WorkoutsEditRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [WorkoutsIndexScreen]
class WorkoutsIndexRoute extends PageRouteInfo<void> {
  const WorkoutsIndexRoute({List<PageRouteInfo>? children})
      : super(
          WorkoutsIndexRoute.name,
          initialChildren: children,
        );

  static const String name = 'WorkoutsIndexRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [TabBarWidgetScreen]
class TabBarWidgetRouter extends PageRouteInfo<void> {
  const TabBarWidgetRouter({List<PageRouteInfo>? children})
      : super(
          TabBarWidgetRouter.name,
          initialChildren: children,
        );

  static const String name = 'TabBarWidgetRouter';

  static const PageInfo<void> page = PageInfo<void>(name);
}
