import 'package:get_storage/get_storage.dart';
import 'package:injectable/injectable.dart';

@module
abstract class GetStorageInjectableModule {
  @Singleton()
  GetStorage get getStorage {
    return GetStorage();
  }
}
