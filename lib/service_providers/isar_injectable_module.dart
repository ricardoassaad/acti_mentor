import 'dart:io';

import 'package:acti_mentor/features/workouts/data/models/category_model.dart';
import 'package:acti_mentor/features/workouts/data/models/exercise_model.dart';
import 'package:injectable/injectable.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';

@module
abstract class IsarInjectableModule {
  @Singleton()
  Future<Isar> get isar async {
    Directory dir = await getApplicationDocumentsDirectory();
    return await Isar.open(
      [CategoryModelSchema, ExerciseModelSchema],
      directory: dir.path,
    );
  }
}
