part of 'theme_bloc.dart';

@freezed
class ThemeState with _$ThemeState {
  const factory ThemeState({
    required ThemeMode? themeModeSetting,
  }) = _ThemeState;

  factory ThemeState.initial() => const ThemeState(
    themeModeSetting: ThemeMode.light,
      );
}
