import 'dart:ui';

class Constants {
  Color primary = const Color(0xFFDD4814);
  Color shade1 = const Color(0xFFec6332);
  Color shade2 = const Color(0xFFee7144);
  Color shade3 = const Color(0xFFef8057);
  Color shade4 = const Color(0xFFf39c7c);
  Color shade5 = const Color(0xFFf5aa8f);
  Color shade6 = const Color(0xFFf6b8a2);
  Color shade7 = const Color(0xFFfad5c7);
  Color shade8 = const Color(0xFFfce3da);
  Color shade9 = const Color(0xFFfdf1ec);
}