import 'package:flutter/material.dart';

import 'constants.dart';

class Themes {
  static final light = ThemeData.from(
    // //TODO: enable when android 12 and above
    useMaterial3: true,
    colorScheme: const ColorScheme.light().copyWith(
      brightness: Brightness.light,
      primary: Constants().primary,
      secondary: Constants().shade3,
    ),
    textTheme: const TextTheme().copyWith(
      displaySmall: TextStyle(
        color: Constants().shade1,
      ),
    ),
  );
  static final dark = ThemeData.from(
    // //TODO: enable when android 12 and above
    useMaterial3: true,
    colorScheme: const ColorScheme.dark().copyWith(
      brightness: Brightness.dark,
      primary: Constants().primary,
      primaryContainer: Constants().primary,
      onPrimaryContainer: Constants().shade9,
      secondary: Constants().shade3,
    ),
    textTheme: const TextTheme().copyWith(
      displaySmall: TextStyle(
        color: Constants().shade1,
      ),
    ),
  );
}
