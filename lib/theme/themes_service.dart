import 'package:acti_mentor/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get_storage/get_storage.dart';

class ThemesService {
  final _box = getIt<GetStorage>();
  final _key = 'themeMode';
  String defaultTheme = "default";

  ThemeMode get theme => _loadTheme() ? ThemeMode.dark : ThemeMode.light;

  bool _loadTheme() {
    //TODO: this service needs to check the settings of the theme from inside the storage
    bool mode = false;
    String storedSettings = _box.read(_key) ?? defaultTheme;
    switch (storedSettings) {
      case "dark":
        mode = true;
        break;
      case "light":
        mode = false;
        break;
      case "default":
      default:
        var brightness = SchedulerBinding.instance.platformDispatcher.platformBrightness;
        mode = brightness == Brightness.dark;
        break;
    }
    return true;
  }

  changeThemeSetting(String themeMode) {
    return _box.write(_key, themeMode);
  }
}

// TODO: implement this enum
enum AppThemeMode {
  dark,
  light,
  systemDefault
}